# Redis仓颉语言客户端用户手册

## 第1章 Redis仓颉语言客户端介绍

### 1.1 简介
仓颉原生Redis客户端。 <br>
依赖库为[Hyperion TCP框架](https://gitcode.com/Cangjie-TPC/hyperion.git)，由[北京宝兰德软件股份有限公司](https://www.bessystem.com)实现。<br>
API设计参考如下项目： <br>
https://github.com/redis/jedis <br>
https://github.com/lettuce-core/lettuce-io

### 1.2 特性
1. 支持RESP2和RESP3协议
2. 接口设计兼容jedis接口语义
3. 丰富的管理命令支持
4. 支持单连接多线程模式
5. 支持发布订阅模式
6. 支持哨兵模式和集群模式
7. 完备的单元测试覆盖
8. 架构简洁，易于扩展

### 1.3 获取Redis仓颉语言客户端
Redis仓颉语言客户端项目托管在gitcode上，项目地址为：<br>
[https://gitcode.com/Cangjie-TPC/redis-sdk/](https://gitcode.com/Cangjie-TPC/redis-sdk/)

使用git下载Redis仓颉语言客户端：<br/>
`> git clone https://gitcode.com/Cangjie-TPC/redis-sdk.git`

### 1.4 通过源码方式引入Redis客户端依赖
仓颉0.53.4以上版本：在项目的cjpm.toml中添加dependencies引入redis_sdk依赖：

```
[dependencies]
  redis_sdk = {git = "https://gitcode.com/Cangjie-TPC/redis-sdk.git", branch = "master", version = "2.0.0"}
```

仓颉0.45.2和0.51.4版本，请参考[Branch_cj0.51.4](https://gitcode.com/Cangjie-TPC/redis-sdk/tree/Branch_cj0.51.4)分支中的说明。

更新依赖，运行cjpm update会自动下载依赖redis_sdk项目到~/.cjpm目录下<br>
`$> cjpm update`

### 1.5 编译Redis仓颉语言客户端
更新依赖，运行cjpm update会自动下载依赖Hyperion TCP框架<br>
`$> cjpm update`

如果更新依赖失败可以参考"1.6 手动下载依赖"

清理工程，在工程根目录下运行：<br>
`$> cjpm clean`

编译工程，在工程根目录下运行：<br>
`$> cjpm build`

编译的静态库位于工程根目录下的如下位置：<br>
`build/release/redis_sdk/redis_sdk.client.api.cjo`<br>
`build/release/redis_sdk/redis_sdk.client.commands.cjo`<br>
`build/release/redis_sdk/redis_sdk.client.commands.impl.cjo`<br>
`build/release/redis_sdk/redis_sdk.client.cjo`<br>
`build/release/hyperion/hyperion.buffer.cjo`<br>
`build/release/hyperion/hyperion.logadapter.cjo`<br>
`build/release/hyperion/hyperion.objectpool.cjo`<br>
`build/release/hyperion/hyperion.threadpool.cjo`<br>
`build/release/hyperion/hyperion.transport.cjo`<br>

### 1.6 手动下载依赖
在redis-sdk工程的根目录下执行：<br/>
`> git clone https://gitcode.com/Cangjie-TPC/hyperion.git`

仓颉0.53.4以上版本：修改cjpm.toml中的dependencies使用本地依赖：

```
[dependencies]
hyperion = { path = "./hyperion"}
```

仓颉0.45.2和0.51.4版本，请参考[Branch_cj0.51.4](https://gitcode.com/Cangjie-TPC/redis-sdk/tree/Branch_cj0.51.4)分支中的说明。

### 1.7 在项目中引入Redis仓颉客户端的静态库依赖

引入编译好的静态库依赖和通过源码方式引入依赖，任意选取一种方式即可。 参考"1.4 通过源码方式引入Redis客户端依赖"。

仓颉0.53.4以上版本，需要先确定平台对应的target-name：<br>

例如Windows X64平台执行`cjc -v`命令返回如下：

```
$cjc -v
Cangjie Compiler: 0.53.4 (cjnative)
Target: x86_64-w64-mingw32
```

例如Linux X64平台执行`cjc -v`命令返回如下：

```
$cjc -v
Cangjie Compiler: 0.53.4 (cjnative)
Target: x86_64-unknown-linux-gnu
```

在工程的cjpm.toml中添加平台对应的二进制依赖，以Linux X64为例：

```
[target.x86_64-unknown-linux-gnu.bin-dependencies.package-option]
  "hyperion.buffer" = "${path_to_redis_sdk}/build/release/hyperion/hyperion.buffer.cjo"
  "hyperion.logadapter" = "${path_to_redis_sdk}/build/release/hyperion/hyperion.logadapter.cjo"
  "hyperion.objectpool" = "${path_to_redis_sdk}/build/release/hyperion/hyperion.objectpool.cjo"
  "hyperion.threadpool" = "${path_to_redis_sdk}/build/release/hyperion/hyperion.threadpool.cjo"
  "hyperion.transport" = "${path_to_redis_sdk}/build/release/hyperion/hyperion.transport.cjo"
  "redis_sdk.client.api" = "${path_to_redis_sdk}/build/release/redis_sdk/redis_sdk.client.api.cjo"
  "redis_sdk.client.commands" = "${path_to_redis_sdk}/build/release/redis_sdk/redis_sdk.client.commands.cjo"
  "redis_sdk.client.commands.impl" = "${path_to_redis_sdk}/build/release/redis_sdk/redis_sdk.client.commands.impl.cjo"
  "redis_sdk.client" = "${path_to_redis_sdk}/build/release/redis_sdk/redis_sdk.client.cjo"
```

仓颉0.45.2和0.51.4版本，请参考[Branch_cj0.51.4](https://gitcode.com/Cangjie-TPC/redis-sdk/tree/Branch_cj0.51.4)分支中的说明。

### 1.8 单元测试
#### 单实例的Redis服务端
修改test/environment.json，将single_instance节点下的host和port更改为Redis服务的监听地址和端口：<br>
```
    "single_instance": {
        "host": "127.0.0.1",
        "port": "6379"
    },
```
其余参数根据需要进行修改。

在工程test/UT目录下运行：<br>
`$> cjpm test`

#### 哨兵模式的高可用Redis服务端
修改test/environment.json，将sentinels节点下的sentinelAddresses修改为哨兵实例监听的地址和端口列表，将sentinels节点下masterName修改为哨兵实例监控的主从实例集群的名称：<br>
```
    "sentinels": {
        "sentinelAddresses": "127.0.0.1:26379,127.0.0.1:26380,127.0.0.1:26381",
        "masterName": "mymaster",
        "sentinelPassword": "",
        "sentinelUser": ""
    },
```
其余参数根据需要进行修改。

在工程test/UT目录下运行：<br>
`$> cjpm test`

#### 集群模式的高可用Redis服务端

修改test/environment.json，将clusters节点下的clusterAddresses修改为集群实例监听的地址和端口列表：<br>

```
    "clusters": {
        "clusterAddresses": "127.0.0.1:7080,127.0.0.1:7081,127.0.0.1:7082,127.0.0.1:7083,127.0.0.1:7084,127.0.0.1:7085"
    },
```

其余参数根据需要进行修改。

在工程test/UT目录下运行：<br>
`$> cjpm test`

## 第2章 单实例的Redis客户端

### 2.1 RedisClient
RedisClient用于连接Redis服务端执行Redis命令。<br>
RedisClient支持执行的命令详见附录2。

### 2.2 RedisClientBuilder
RedisClientBuilder用于创建RedisClient， 获取RedisClientBuilder可以使用如下工具方法：

| RedisClientBuilder类                                                   | 作用                                                    |
|---------------------------------------------------------------------------- |---------------------------------------------------------|
| static func builder(): RedisClientBuilder                   | 创建RedisClientBuilder                       |
| static func builder(RedisClientConfig): RedisClientBuilder  | 使用RedisClientConfig创建RedisClientBuidler   |

在RedisClientBuilder对象实例上设置相关属性后，调用build方法创建RedisClient：

| RedisClientBuilder类                     | 作用                       |
|-----------------------------------------------|----------------------------|
| func build(): RedisClient           | 创建RedisClient       |


为了便捷创建RedisClient，RedisClientBuilder提供了如下工具方法：

| RedisClientBuilder类                                                                                                        | 作用                                                           |
|----------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------|
| static func buildRedisClient(host: String, port: UInt16): RedisClient                                  | 提供Redis服务的主机名和端口，创建RedisClient           |
| static func buildRedisClient(host: String, port: UInt16, password: String): RedisClient                | 提供Redis服务的主机名、端口和密码，创建RedisClient       |
| static func buildRedisClient(host: String, port: UInt16, user: String, password: String): RedisClient  | 提供Redis服务的主机名、端口、用户名密码，创建RedisClient  |

### 2.3 创建RedisClient
```cangjie
import redis_sdk.client.api.*
import redis_sdk.client.commands.*
import redis_sdk.client.*

public func getRedisClient(): RedisClient {
    let redisClient = RedisClientBuilder.builder()
        .host("127.0.0.1")
        .port(6379)
        .readTimeout(Duration.second * 60)
        .writeTimeout(Duration.second * 30)
        .receiveBufferSize(32768)
        .sendBufferSize(32768)
        .build()
    return redisClient
}
```

### 2.4 使用RedisClient执行Redis命令操作
```cangjie
import redis_sdk.client.api.*
import redis_sdk.client.commands.*
import redis_sdk.client.*

main() {
    // 获取Redis客户端
    let client = getRedisClient()
    // 执行 SET testKey1 testValue1 XX命令
    let result = client.set("testKey1", "testValue1", SetParams().xx())
    if (let Some(result) <- result) {
        println(result)
    } else {
        println("nil")
    }

    // 执行 GET testKey1命令
    var getResult = client.get("testKey1")
    if (let Some(getResult) <- getResult) {
        println(getResult)
    } else {
        println("nil")
    }
}
```

## 第3章 订阅模式的客户端
### 3.1 RedisSubscriber
RedisSubscriber用于订阅发布消息的频道，支持阻塞订阅和非阻塞订阅：

| RedisSubscriber类                                             | 作用                                                                                              |
|-------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| func subscribe(channels: Array&lt;String&gt;)       | 订阅channels指定的频道列表                                                                      |
| func blockSubscribe(channels: Array&lt;String&gt;)  | 阻塞方式订阅channels指定的频道列表，当订阅频道数为0时或者客户端关闭时退出                       |
| func psubscribe(patterns: Array&lt;String&gt;)      | 使用模式匹配方式订阅匹配patterns的频道列表                                                      |
| func blockPsubscribe(patterns: Array&lt;String&gt;) | 阻塞方式使用模式匹配方式订阅匹配patterns的频道列表，当订阅频道数为0时或者客户端关闭时退出       |
| func unsubscribe()                                  | 取消订阅所有频道                                                                                  |
| func unsubscribe(channels: Array&lt;String&gt;)     | 取消订阅channels指定的频道列表                                                                  |
| func punsubscribe()                                 | 取消订阅所有使用模式匹配方式订阅的频道                                                            |
| func punsubscribe(patterns: Array&lt;String&gt;)    | 取消订阅匹配patterns频道                                                                        |
| func ping()                                         | 发送PING消息                                                                                     |
| func ping(message: String)                          | 发送PING message消息                                                                           |

### 3.2 RedisSubscriberBuilder
RedisSubscriberBuilder用于创建RedisSubscriber， 获取RedisSubscriber可以使用如下工具方法：

| RedisSubscriberBuilder类                                                   | 作用                                                           |
|----------------------------------------------------------------------------------|----------------------------------------------------------------|
| static func builder(): RedisSubscriberBuilder                   | 创建RedisSubscriberBuilder                         |
| static func builder(RedisClientConfig): RedisSubscriberBuilder  | 使用RedisClientConfig创建RedisSubscriberBuilder     |

在RedisSubscriberBuilder对象实例上设置相关属性后，调用build方法创建RedisSubscriber：

| RedisSubscriberBuilder类                    | 作用                          |
|----------------------------------------------------|-------------------------------|
| func build(): RedisSubscriber          | 创建RedisSubscriber     |


为了便捷创建RedisSubscriber，RedisSubscriberBuilder提供了如下工具方法：

| RedisSubscriberBuilder类                                                                                                                 | 作用                                                              |
|-------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------|
| static func buildRedisSubscriber(host: String, port: UInt16): RedisSubscriber                                 | 提供Redis服务的主机名和端口，创建RedisClient              |
| static func buildRedisSubscriber(host: String, port: UInt16, password: String): RedisSubscriber               | 提供Redis服务的主机名、端口和密码，创建RedisClient         |
| static func buildRedisSubscriber(host: String, port: UInt16, user: String, password: String): RedisSubscriber | 提供Redis服务的主机名、端口、用户名密码，创建RedisClient    |

### 3.3 SubscriberListener接口
为了处理订阅客户端收到的消息，需要向RedisSubscriber设置一个SubscriberListener的实现。下面是一个SubscriberListener的实现示例：
```cangjie
import redis_sdk.client.api.*
import redis_sdk.client.commands.*
import redis_sdk.client.*

public class PrintSubscriberListener <: SubscriberListener {
    public func onMessage(channel: String, message: String): Unit {
        println("Receive mesage: ${message} from channel ${channel}")
    }

    public func onPMessage(pattern: String, channel: String, message: String): Unit {
        println("Receive pmesage: ${message} from channel ${channel}, pattern: ${pattern}")
    }

    public func onSubscribe(channel: String, subscribedChannels: Int64): Unit {
        println("Subcribe channel ${channel},  subscribedChannels: ${subscribedChannels}")
    }

    public func onUnsubscribe(channel: String, subscribedChannels: Int64): Unit {
        println("Unsubscribe channel ${channel},  subscribedChannels: ${subscribedChannels}")
    }

    public func onPUnsubscribe(pattern: String, subscribedChannels: Int64): Unit {
        println("PUnsubcribe pattern ${pattern},  subscribedChannels: ${subscribedChannels}")
    }

    public func onPSubscribe(pattern: String, subscribedChannels: Int64): Unit {
        println("PSubscribe pattern ${pattern},  subscribedChannels: ${subscribedChannels}")
    }

    public func onPong(message: String): Unit {
        println("Receive pong mesage: ${message}")
    }

    public func onExceptionCaught(ex: Exception): Unit {
        println("Caught exception: " + ex.message)
        ex.printStackTrace()
    }
}
```

### 3.4 SubscriberAwareListener类
如果在处理订阅消息的过程中，需要取消或者增加订阅的频道，可以继承SubscriberAwareListener类来处理相关逻辑。SubscriberAwareListener类的声明如下：
```cangjie
public abstract class SubscriberAwareListener <: SubscriberListener {
    private let subscriber: Subscriber

    public init(subscriber: Subscriber) {
        this.subscriber = subscriber
    }

    public func getSubscriber() {
        return this.subscriber
    }

    public func subscribe(channels: Array<String>): Unit {
        subscriber.subscribe(channels)
    }

    public func psubscribe(patterns: Array<String>): Unit {
        subscriber.psubscribe(patterns)
    }

    public func unsubscribe(): Unit {
        subscriber.unsubscribe()
    }

    public func unsubscribe(channels: Array<String>): Unit {
        subscriber.unsubscribe(channels)
    }

    public func punsubscribe(): Unit {
        subscriber.punsubscribe()
    }

    public func punsubscribe(patterns: Array<String>): Unit {
        subscriber.punsubscribe(patterns)
    }
}
```

### 3.5 创建RedisSubscriber
```cangjie
import redis_sdk.client.api.*
import redis_sdk.client.commands.*
import redis_sdk.client.*

public func getRedisSubscriber(): RedisSubscriber {
    let redisSubscriber = RedisSubscriberBuilder.builder()
        .host("127.0.0.1")
        .port(6379)
        .password("mypassword")
        // 后台线程拉取订阅消息的超时时间，超时后继续尝试下一次拉取
        .checkSubscribeMessageInterval(Duration.second * 3) 
        .build()
    redisSubscriber.setSubscriberListener(PrintSubscriberListener())
    return redisSubscriber
}
```

### 3.6 使用RedisSubscriber订阅消息
```cangjie
import redis_sdk.client.api.*
import redis_sdk.client.commands.*
import redis_sdk.client.*

main() {
    let redisSubscriber = getRedisSubscriber()
    // 订阅FirstChannel和SecondChannel频道，非阻塞方式，订阅后立即返回
    redisSubscriber.subscribe("FirstChannel", "SecondChannel")
    // 使用模式匹配方式订阅以MyGroup开头的频道，非阻塞方式，订阅后立即返回
    redisSubscriber.psubscribe("MyGroup*")

    // 订阅ThirdChannel，阻塞方式，订阅后等到服务端返回的订阅频道数为0，
    // 或者redisSubscriber关闭时退出
    redisSubscriber.blockPsubscribe("ThirdChannel")
}
```

运行程序后，使用RedisClient或者redis-cli向FirstChannel、SecondChanel、ThirdChannel、MyGroupChannelA、MyGroupChannelB发送消息，会打印类似消息：<br>
```shell
Subcribe channel FirstChannel,  subscribedChannels: 1
Subcribe channel SecondChannel,  subscribedChannels: 2
PSubscribe pattern MyGroup*,  subscribedChannels: 3
PSubscribe pattern ThirdChannel,  subscribedChannels: 4
Receive mesage: FirstMessage from channel FirstChannel
Receive mesage: SecondMessage from channel SecondChannel
Receive pmesage: ThirdMessage from channel ThirdChannel, pattern: ThirdChannel
Receive pmesage: GroupMessage1 from channel MyGroupChannalA, pattern: MyGroup*
Receive pmesage: GroupMessage2 from channel MyGroupChannalB, pattern: MyGroup*
```

### 3.7 支持恢复的RedisSubscriber
#### RecoverableRedisSubscriber
RecoverableRedisSubscriber为支持连接故障恢复的订阅客户端，当连接断开后会尝试自动恢复

#### RecoverableRedisSubscriberBuilder
RedisSubscriberBuilder用于创建RedisSubscriber， 获取RedisSubscriber可以使用如下工具方法：

| RecoverableRedisSubscriberBuilder类                                            | 作用                                                                       |
|-----------------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| static func builder(): RecoverableRedisSubscriber                     | 创建RecoverableRedisSubscriberBuilder                       |
| static func builder(RedisClientConfig): RecoverableRedisSubscriber    | 使用RedisClientConfig创建RecoverableRedisSubscriberBuilder   |

在RecoverableRedisSubscriberBuilder对象实例上设置相关属性后，调用build方法创建RecoverableRedisSubscriber：

| RecoverableRedisSubscriberBuilder类             | 作用                              |
|-------------------------------------------------|-----------------------------------|
| func build(): RecoverableRedisSubscriber        | 创建RecoverableRedisSubscriber    |


为了便捷创建RedisSubscriber，RedisSubscriberBuilder提供了如下工具方法：

| RecoverableRedisSubscriberBuilder类                                                                                                                          | 作用                                                                                  |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------|
| static func buildRecoverableRedisSubscriber(host: String, port: UInt16): RecoverableRedisSubscriber                                 | 提供Redis服务的主机名和端口，创建RecoverableRedisSubscriber            |
| static func buildRecoverableRedisSubscriber(host: String, port: UInt16, password: String): RecoverableRedisSubscriber               | 提供Redis服务的主机名、端口和密码，创建RecoverableRedisSubscriber       |
| static func buildRecoverableRedisSubscriber(host: String, port: UInt16, user: String, password: String): RecoverableRedisSubscriber | 提供Redis服务的主机名、端口、用户名密码，创建RecoverableRedisSubscriber  |

#### 创建支持故障恢复的RedisSubscriber，当连接断开后可自动恢复
```cangjie
public func getRecoverableRedisSubscriber(): RedisSubscriber {
    let redisSubscriber = RecoverableRedisSubscriberBuilder.builder()
        .host("127.0.0.1")
        .port(6379)
        .password("mypassword")
        // 后台线程拉取订阅消息的超时时间，超时后继续尝试下一次拉取
        .checkSubscribeMessageInterval(Duration.second * 3) 
        // 连接故障时，恢复RedisSubscriber的最大等待时间
        .maxSubscriberRecoverDuration(Duration.minute * 20)
        // 连接故障时，每分钟尝试的恢复次数
        .recoverAttemptsPerMinute(3)
        .build()
    redisSubscriber.setSubscriberListener(PrintSubscriberListener())
    return redisSubscriber
}
```
## 第4章 哨兵模式客户端
### 4.1 SentinelRedisClientBuilder
SentinelRedisClientBuilder用于创建连接哨兵模式的master实例的RedisClient， 获取SentinelRedisClientBuilder可以使用如下工具方法：

| SentinelRedisClientBuilder类                                 | 作用                                               |
| ------------------------------------------------------------ | -------------------------------------------------- |
| static func builder(masterName: String): SentinelRedisClientBuilder | 传入masterName，创建SentinelRedisClientBuilder     |
| static func builder(sentinelConfig: SentinelRedisClientConfig): SentinelRedisClientBuilder | 传入sentinelConfig，创建SentinelRedisClientBuilder |

在SentinelRedisClientBuilder对象实例上设置相关属性后，调用build方法创建UnifiedRedisClient：

| SentinelRedisClientBuilder类            | 作用                           |
|------------------------------------------------|--------------------------------|
| func build(): UnifiedRedisClient | 创建UnifiedRedisClient |


为了便捷创建UnifiedRedisClient，RedisClientBuilder提供了如下工具方法：
```cangjie
    // 提供主从集群的名称和哨兵实例的地址列表创建连接master实例的UnifiedRedisClient
    static func buildSentineledClient(
       masterName: String, 
       hostAndPorts: Array<HostAndPort>): UnifiedRedisClient
    
    // 提供主从集群的名称、哨兵实例的密码和哨兵实例的地址列表创建连接master实例的UnifiedRedisClient
    static func buildSentinelAuthClient(
        masterName: String,
        sentinelPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): UnifiedRedisClient
    
    // 提供主从集群的名称、哨兵实例的用户名，哨兵实例的密码和哨兵实例的地址列表创建连接master实例的UnifiedRedisClient
    static func buildSentinelAuthClient(
        masterName: String,
        sentinelUser: String,
        sentinelPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): UnifiedRedisClient
    
    // 提供主从集群的名称、主实例的密码和哨兵实例的地址列表创建连接master实例的UnifiedRedisClient
    static func buildMasterAuthClient(
        masterName: String,
        masterPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): UnifiedRedisClient
    
    // 提供主从集群的名称、主实例的用户名、主实例的密码和哨兵实例的地址列表创建连接master实例的UnifiedRedisClient
    static func buildMasterAuthClient(
        masterName: String,
        masterUser: String,
        masterPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): UnifiedRedisClient
    
    // 提供主从集群的名称、主实例的密码、哨兵实例的密码和哨兵实例的地址列表创建连接master实例的UnifiedRedisClient
    static func buildSentinelAndMasterAuthClient(
        masterName: String,
        masterPassword: String,
        sentinelPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): UnifiedRedisClient

    // 提供主从集群的名称、主实例的用户名、主实例的密码、哨兵实例的用户名、哨兵实例的密码和哨兵实例的地址列表创建连接master实例的UnifiedRedisClient
    static func buildSentinelAndMasterAuthClient(
        masterName: String,
        sentinelUser: String,
        sentinelPassword: String,
        masterUser: String,
        masterPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): UnifiedRedisClient 
```

### 4.2 使用哨兵模式时，创建连接Master实例的UnifiedRedisClient
连接哨兵模式的Redis集群，需要提供哨兵实例的地址列表，并创建连接到master实例的连接用于执行Redis命令。<br>
当master实例发生变化时，Redis客户端会监听到新的master实例的地址，并创建连接到新的master实例的连接。<br>

```cangjie
import redis_sdk.client.api.*
import redis_sdk.client.*

public func getSentinelRedisClient(): UnifiedRedisClient {
    let redisClient = SentinelRedisClientBuilder
        // 哨兵监控的主从集群的名称
        .builder("mymaster")
        // master实例的认证密码
        .masterPassword("mypassword")
        // 添加哨兵实例地址
        .addSentinelHostAndPort("127.0.0.1", 26379)
        .addSentinelHostAndPort("127.0.0.1", 26380)
        .addSentinelHostAndPort("127.0.0.1", 26381)
        .build()
    return redisClient
}
```

### 4.3 使用哨兵模式时，创建连接Master实例的RedisSubscriber

#### SentinelRedisSubscriberBuilder
SentinelRedisSubscriberBuilder用于创建连接哨兵模式的master实例的RedisSubscriber， 获取SentinelRedisSubscriberBuilder可以使用如下工具方法：

| SentinelRedisSubscriberBuilder类                             | 作用                                                   |
| ------------------------------------------------------------ | ------------------------------------------------------ |
| static func builder(masterName: String): SentinelRedisSubscriberBuilder | 传入masterName，创建SentinelRedisSubscriberBuilder     |
| static func builder(sentinelConfig: SentinelRedisClientConfig): SentinelRedisSubscriberBuilder | 传入sentinelConfig，创建SentinelRedisSubscriberBuilder |

在SentinelRedisSubscriberBuilder对象实例上设置相关属性后，调用build方法创建RedisSubscriber：

| SentinelRedisClientBuilder类            | 作用                          |
|------------------------------------------------|-------------------------------|
| func build(): RedisSubscriber | 创建RedisSubscriber |

为了便捷创建RedisSubscriber，SentinelRedisSubscriberBuilder提供了如下工具方法：
```cangjie
    // 提供主从集群的名称和哨兵实例的地址列表创建连接master实例的RedisSubscriber
    static func buildSentineledSubscriber(
       masterName: String, 
       hostAndPorts: Array<HostAndPort>): RedisSubscriber
    
    // 提供主从集群的名称、哨兵实例的密码和哨兵实例的地址列表创建连接master实例的RedisSubscriber
    static func buildSentinelAuthSubscriber(
        masterName: String,
        sentinelPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): RedisSubscriber
    
    // 提供主从集群的名称、哨兵实例的用户名，哨兵实例的密码和哨兵实例的地址列表创建连接master实例的RedisSubscriber
    static func buildSentinelAuthSubscriber(
        masterName: String,
        sentinelUser: String,
        sentinelPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): RedisSubscriber
    
    // 提供主从集群的名称、主实例的密码和哨兵实例的地址列表创建连接master实例的RedisSubscriber
    static func buildMasterAuthSubscriber(
        masterName: String,
        masterPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): RedisSubscriber
    
    // 提供主从集群的名称、主实例的用户名、主实例的密码和哨兵实例的地址列表创建连接master实例的RedisSubscriber
    static func buildMasterAuthSubscriber(
        masterName: String,
        masterUser: String,
        masterPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): RedisSubscriber
    
    // 提供主从集群的名称、主实例的密码、哨兵实例的密码和哨兵实例的地址列表创建连接master实例的RedisSubscriber
    static func buildSentinelAndMasterAuthSubscriber(
        masterName: String,
        masterPassword: String,
        sentinelPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): RedisSubscriber

    // 提供主从集群的名称、主实例的用户名、主实例的密码、哨兵实例的用户名、哨兵实例的密码和哨兵实例的地址列表创建连接master实例的RedisSubscriber
    static func buildSentinelAndMasterAuthSubscriber(
        masterName: String,
        sentinelUser: String,
        sentinelPassword: String,
        masterUser: String,
        masterPassword: String,
        hostAndPorts: Array<HostAndPort>
    ): RedisSubscriber 
```

#### 使用哨兵模式时，创建连接Master实例的RedisSubscriber
```cangjie
import redis_sdk.client.api.*
import redis_sdk.client.*

public func getSentinelRedisSubscriber(): RedisSubscriber {
    let redisSubscriber = SentinelRedisSubscriberBuilder
        // 哨兵监控的主从集群的名称
        .builder("mymaster")
        // master实例的认证密码
        .masterPassword("mypassword")
        // 添加哨兵实例地址
        .addSentinelHostAndPort("127.0.0.1", 26379)
        .addSentinelHostAndPort("127.0.0.1", 26380)
        .addSentinelHostAndPort("127.0.0.1", 26381)
        .build()
    return redisSubscriber
}
```

## 第5章 集群模式客户端

### 5.1 ClusterRedisClientBuilder
| ClusterRedisClientBuilder类                                  | 作用                                             |
| ------------------------------------------------------------ | ------------------------------------------------ |
| static func builder(): ClusterRedisClientBuilder             | 创建ClusterRedisClientBuilder                    |
| static func builder(clusterConfig: ClusterRedisClientConfig): ClusterRedisClientBuilder | 传入clusterConfig，创建ClusterRedisClientBuilder |

在ClusterRedisClientBuilder对象实例上设置相关属性后，调用build方法创建RedisClient：

| ClusterRedisClientBuilder类     | 作用                           |
|------------------------------------------------|--------------------------------|
| func build(): ClusterRedisClient | 创建ClusterRedisClient |
为了便捷创建ClusterRedisClient，ClusterRedisClientBuilder提供了如下工具方法：

```cangjie
    // 提供集群其中一个实例的主机名和端口用于创建集群客户端ClusterRedisClient
    public static func buildClusterRedisClient(host: String, port: UInt16): ClusterRedisClient {
        let clientBuilder = builder()
        return clientBuilder.addClusterHostAndPort(host, port).build()
    }
    // 提供集群实例密码、其中一个实例的主机名和端口用于创建集群客户端ClusterRedisClient
    public static func buildClusterRedisClient(password: String, host: String, port: UInt16): ClusterRedisClient {
        let clientBuilder = builder()
        return clientBuilder.addClusterHostAndPort(host, port).password(password).build()
    }
    // 提供集群实例用户名、密码、其中一个实例的主机名和端口用于创建集群客户端ClusterRedisClient
    public static func buildClusterRedisClient(user: String, password: String, host: String, port: UInt16): ClusterRedisClient {
        let clientBuilder = builder()
        return clientBuilder.addClusterHostAndPort(host, port).user(user).password(password).build()
    }
    // 提供集群的实例地址列表用于创建集群客户端ClusterRedisClient
    public static func buildClusterRedisClient(hostAndPorts: Array<HostAndPort>): ClusterRedisClient {
        let clientBuilder = builder()
        clientBuilder.addClusterHostAndPorts(ArrayList<HostAndPort>(hostAndPorts))
        return clientBuilder.build()
    }
    // 提供集群实例密码、实例地址列表用于创建集群客户端ClusterRedisClient
    public static func buildClusterRedisClient(password: String, hostAndPorts: Array<HostAndPort>): ClusterRedisClient {
        let clientBuilder = builder()
        return clientBuilder.addClusterHostAndPorts(ArrayList<HostAndPort>(hostAndPorts)).password(password).build()
    }
    // 提供集群实例用户名、密码、实例地址列表用于创建集群客户端ClusterRedisClient
    public static func buildClusterRedisClient(user: String, password: String, hostAndPorts: Array<HostAndPort>): ClusterRedisClient {
        let clientBuilder = builder()
        return clientBuilder.addClusterHostAndPorts(ArrayList<HostAndPort>(hostAndPorts)).user(user).password(password).
            build()
    }
```



### 5.2 使用集群模式时，创建连接集群实例的ClusterRedisClient

连接集群模式的Redis集群，需要提供集群实例的地址列表，并创建连接到集群实例的连接用于执行Redis命令。

```cangjie
import redis_sdk.client.api.*
import redis_sdk.client.*

public func getClusterRedisClient(): ClusterRedisClient {
	let clusterRedisClient = ClusterRedisClientBuilder
			.builder()
			// 添加集群实例地址
			.addClusterHostAndPort("127.0.0.1", 7080)
			.addClusterHostAndPort("127.0.0.1", 7081)
			.addClusterHostAndPort("127.0.0.1", 7082)
			.addClusterHostAndPort("127.0.0.1", 7083)
			.addClusterHostAndPort("127.0.0.1", 7084)
			.addClusterHostAndPort("127.0.0.1", 7085)
            .build()
     return clusterRedisClient
}
```

### 5.3 使用集群模式时，创建连接集群实例的RedisSubscriber
#### ClusterRedisSubscriberBuilder
ClusterRedisSubscriberBuilder用于创建连接集群模式的RedisSubscriber， 获取ClusterRedisSubscriberBuilder可以使用如下工具方法：

| ClusterRedisSubscriberBuilder类                              | 作用                                                 |
| ------------------------------------------------------------ | ---------------------------------------------------- |
| static func builder(): ClusterRedisSubscriberBuilder         | 创建ClusterRedisSubscriberBuilder                    |
| static func builder(clusterConfig: ClusterRedisClientConfig): ClusterRedisSubscriberBuilder | 传入clusterConfig，创建ClusterRedisSubscriberBuilder |

在ClusterRedisSubscriberBuilder对象实例上设置相关属性后，调用build方法创建RedisSubscriber：

| ClusterRedisSubscriberBuilder类 | 作用                          |
|------------------------------------------------|-------------------------------|
| func build(): RedisSubscriber | 创建RedisSubscriber |

为了便捷创建RedisSubscriber，ClusterRedisSubscriberBuilder提供了如下工具方法：

```cangjie
	// 提供集群其中一个实例的主机名和端口用于创建集群RedisSubscriber
    public static func buildClusterRedisSubscriber(host: String, port: UInt16): RedisSubscriber {
        let clientBuilder = builder()
        return clientBuilder.addClusterHostAndPort(host, port).build()
    }
	// 提供集群实例密码、其中一个实例的主机名和端口用于创建集群RedisSubscriber
    public static func buildClusterRedisSubscriber(password: String, host: String, port: UInt16): RedisSubscriber {
        let clientBuilder = builder()
        return clientBuilder.addClusterHostAndPort(host, port).password(password).build()
    }
	// 提供集群实例用户名、密码、其中一个实例的主机名和端口用于创建集群RedisSubscriber
    public static func buildClusterRedisSubscriber(user: String, password: String, host: String, port: UInt16): RedisSubscriber {
        let clientBuilder = builder()
        return clientBuilder.addClusterHostAndPort(host, port).user(user).password(password).build()
    }
	// 提供集群的实例地址列表用于创建集群RedisSubscriber
    public static func buildClusterRedisSubscriber(hostAndPorts: Array<HostAndPort>): RedisSubscriber {
        let clientBuilder = builder()
        clientBuilder.addClusterHostAndPorts(ArrayList<HostAndPort>(hostAndPorts))
        return clientBuilder.build()
    }
	// 提供集群实例密码、实例地址列表用于创建集群RedisSubscriber
    public static func buildClusterRedisSubscriber(password: String, hostAndPorts: Array<HostAndPort>): RedisSubscriber {
        let clientBuilder = builder()
        return clientBuilder.addClusterHostAndPorts(ArrayList<HostAndPort>(hostAndPorts)).password(password).build()
    }
	// 提供集群实例用户名、密码、实例地址列表用于创建集群RedisSubscriber
    public static func buildClusterRedisSubscriber(user: String, password: String, hostAndPorts: Array<HostAndPort>): RedisSubscriber {
        let clientBuilder = builder()
        return clientBuilder.addClusterHostAndPorts(ArrayList<HostAndPort>(hostAndPorts)).user(user).password(password).
            build()
    }
```



#### 使用集群模式时，创建连接集群实例的RedisSubscriber

```cangjie
import redis_sdk.client.api.*
import redis_sdk.client.*

public func getClusterRedisSubscriber(): RedisSubscriber {
    let redisSubscriber = ClusterRedisSubscriberBuilder
        .builder()
        // 添加集群实例地址
        .addClusterHostAndPort("127.0.0.1", 7080)
        .addClusterHostAndPort("127.0.0.1", 7081)
        .addClusterHostAndPort("127.0.0.1", 7082)
        .addClusterHostAndPort("127.0.0.1", 7083)
        .addClusterHostAndPort("127.0.0.1", 7084)
        .addClusterHostAndPort("127.0.0.1", 7085)
        .build()
    return redisSubscriber
}
```



## 附录

### 附录1. Redis仓颉语言客户端支持的配置

#### 1.1 通用配置

##### Redis服务配置

| RedisClientBuilder/RedisSubscriberBuidler类的方法 | 作用                                  | 默认值         |
|---------------------------------------------------|---------------------------------------|----------------|
| func host(String)                                 | 设置Redis服务的监听地址               |                |
| func port(UInt16)                                 | 设置Redis服务的监听端口               |                |
| func respVersion(ProtocolVersion)                 | 设置使用的RESP协议，支持2和3          |                |
| func respVersion(Int64)                           | 设置使用的RESP协议，支持2和3          |                |
| func user(String)                                 | 设置连接Redis服务的用户名             |                |
| func password(Array&lt;Byte&gt;)                  | 设置连接Redis服务的密码               |                |
| func password(String)                             | 设置连接Redis服务的密码               |                |
| func clientName(String)                           | 设置连接Redis服务的客户端名称         |                |
| func waitResponseTimeout(Duration)                | 客户端等待Redis命令执行完成的时间     |  5分钟         |

##### Socket配置

| RedisClientBuilder/RedisSubscriberBuidler类的方法 | 作用                                  | 默认值         |
|---------------------------------------------------|---------------------------------------|----------------|
| func receiveBufferSize(Int64)                     | 设置客户端Socket连接的接收缓冲区大小  |                |
| func sendBufferSize(Int64)                        | 设置客户端Socket连接的发送缓冲区大小  |                |
| func noDelay(Bool)                                | 设置客户端Socket连接的TCP_NODELAY     |                |
| func linger(Duration)                             | 设置客户端Socket连接的SO_LINGER       |                |
| func readTimeout(Duration)                        | 设置客户端Socket连接的读超时时间      |                |
| func writeTimeout(Duration)                       | 设置客户端Socket连接的写超时时间      |                |

##### 线程池配置

| RedisClientBuilder/RedisSubscriberBuidler类的方法 | 作用                                  | 默认值         |
|---------------------------------------------------|---------------------------------------|----------------|
| func threadPoolName(String)                       | 设置客户端使用的线程池的名称          | RedisClient    |
| func threadPoolQueueSize(Int64)                   | 设置客户端线程池的队列大小            | 4096           |
| func minThreads(Int64)                            | 设置客户端线程池的最小线程数          | 0              |
| func maxThreads(Int64)                            | 设置客户端线程池的最大线程数          | 256             |
| func threadIdleTimeout(Duration)                  | 设置客户端线程池的线程空闲超时时间    | 2分钟          |

##### 连接池配置

| RedisClientBuilder/RedisSubscriberBuidler类的方法  | 作用                                      | 默认值         |
|----------------------------------------------------|-------------------------------------------|----------------|
| func idleTimeout(Duration)                         | 设置连接的空闲超时时间                    | 20分钟         |
| func waitConnectionTimeout(Duration)               | 设置等待连接的最大超时时间                | 30秒           |
| func testOnCreate(Bool)                            | 创建连接时检测连接是否有效                | false          |
| func testOnBorrow(Bool)                            | 借出连接时检测连接是否有效                | true           |
| func testOnReturn(Bool)                            | 归还连接时检测连接是否有效                | false          |
| func testWhileIdle(Bool)                           | 连接空闲时检测连接是否有效                | false          |
| func objectPoolEvictionIntervals(Duration)         | 检测连接池的时间间隔                      | None           |
| func objectPoolLifo(objectPoolLifo: Bool)          | 连接池中的连接是否使用后进先出的出队顺序  | true           |

##### Hyperion TCP框架配置

| RedisClientBuilder/RedisSubscriberBuidler类的方法  | 作用                                      | 默认值         |
|----------------------------------------------------|-------------------------------------------|----------------|
| func asyncWrite(Bool)                              | 是否开启每个连接一个写线程                | true           |
| func sliceExceedBuffer(Bool)                       | 是否通过切片方式减少数组拷贝              | true           |
| func maxMessageSizeInBytes(Int64)                  | 支持处理的消息的最大长度                  | 64M            |
| func bufferAllocateSize(Int64)                     | 分配的ByteBuffer大小                      | 8192           |

##### Hyperion TCP框架实验性质配置

实验性质配置能提升性能，目前不建议在生产环境中修改这些配置的默认值

| RedisClientBuilder/RedisSubscriberBuidler类的方法  | 作用                                      | 默认值         |
|----------------------------------------------------|-------------------------------------------|----------------|
| * func useNonExclusiveObjectPool(Bool)             | 是否允许借出的连接被多个线程同时使用      | false          |
| * func usePooledBufferAllocator(Bool)              | 是否将ByteBuffer池化重用                  | false          |
| * func maxPooledBuffers(Int64)                     | 缓存的ByteBuffer的最大数量                | 2048           |

#### 1.2 订阅客户端RedisSubscriber特有的配置

| RedisSubscriberBuilder类的方法                | 作用                                                    | 默认值  |
|-----------------------------------------------|---------------------------------------------------------|---------|
| func checkSubscribeMessageInterval(Duration)  | 后台线程拉取订阅消息的超时时间，超时后尝试下一次拉取    | 3秒钟   |


#### 1.3 持久化订阅客户端RecoverableRedisSubscriber特有的配置

| RecoverableRedisSubscriberBuilder类的方法      | 作用                                  |  默认值   |
|------------------------------------------------|---------------------------------------|-----------|
| func maxSubscriberRecoverDuration(Duration)    | 连接故障时，恢复连接的最大等待时间    | 20分钟    |
| func recoverAttemptsPerMinute(Int64)           | 连接故障时，每分钟尝试恢复连接的次数  | 3         |

#### 1.4 哨兵模式客户端特有的配置

| SentinelRedisClientBuilder类的方法                           | 作用                                        |  默认值   |
|--------------------------------------------------------------|---------------------------------------------|-----------|
| func addSentinelHostAndPort(HostAndPort)                     | 添加哨兵实例的主机名和端口                  |           |
| func addSentinelHostAndPort(String, UInt16)                  | 添加哨兵实例的主机名和端口                  |           |
| func addSentinelHostAndPort(Array&lt;HostAndPort&gt;)        | 添加多个哨兵实例的主机名和端口列表          |           |
| func addSentinelHostAndPort(ArrayList&lt;HostAndPort&gt;)    | 添加多个哨兵实例的主机名和端口列表          |           |
| func sentinelUser(String)                                    | 设置连接哨兵实例的认证使用的用户名          |           |
| func sentinelPassword(Array&lt;Byte&gt;)                     | 设置连接哨兵实例的认证使用的密码            |           |
| func sentinelPassword(String)                                | 设置连接哨兵实例的认证使用的密码            |           |
| func masterUser(String)                                      | 设置连接Master实例的认证使用的用户名        |           |
| func masterPassword(Array&lt;Byte&gt;)                       | 设置连接Master实例的认证使用的密码          |           |
| func masterPassword(String)                                  | 设置连接Master实例的认证使用的密码          |           |
| func maxAttempts(Int64)                                      | 连接故障时，尝试重新发送命令的次数          | 1，不重试 |
| func maxTotalRetriesDuration(Duration)                       | 连接故障时，尝试重新发送命令的最大等待时间  | 30秒钟    |

#### 1.5 集群模式客户端特有的配置

| SentinelRedisClientBuilder类的方法                           | 作用                                            |  默认值   |
|--------------------------------------------------------------|-------------------------------------------------|-----------|
| func addClusterHostAndPort(HostAndPort)                      | 添加集群实例的主机名和端口                      |           |
| func addClusterHostAndPort(String, UInt16)                   | 添加集群实例的主机名和端口                      |           |
| func addClusterHostAndPort(Array&lt;HostAndPort&gt;)         | 添加多个集群实例的主机名和端口列表              |           |
| func addClusterHostAndPort(ArrayList&lt;HostAndPort&gt;)     | 添加多个集群实例的主机名和端口列表              |           |
| func maxAttempts(Int64)                                      | 执行命令出错时，尝试重新发送命令的次数          | 5         |
| func maxTotalRetriesDuration(Duration)                       | 执行命令出错时，尝试重新发送命令的最大等待时间  | 30秒钟    |
| func topologyRefreshPeriod(Duration)                         | 定时更新槽位缓存信息的时间                      | None      |

### 附录2. 支持的命令列表

#### BitMap数据类型操作命令
| 命令        | RedisClient类的方法                                          |
| ----------- | :----------------------------------------------------------- |
| BITCOUNT    | func bitcount(key: String): Int64                            |
|             | func bitcount(key: String, start: Int64, end: Int64): Int64  |
|             | func bitcount(key: String, start: Int64, end: Int64, option: BitCountOption): Int64 |
| BITFIELD    | func bitfield(key: String, arguments: Array&lt;String&gt;): ?ArrayList&lt;Int64&gt; |
| BITFIELD_RO | func bitfieldReadonly(key: String, arguments: Array&lt;String&gt;): ArrayList&lt;Int64&gt; |
| BITOP       | func bitop(op: BitOP, destKey: String, srcKeys: Array&lt;String&gt;): Int64 |
| BITPOS      | func bitpos(key: String, value: Bool): Int64                 |
|             | func bitpos(key: String, value: Bool, params: BitPosParams): Int64 |
| GETBIT      | func getbit(key: String, offset: Int64): Bool                |
| SETBIT      | func setbit(key: String, offset: Int64, value: Bool): Bool   |

#### 地理空间坐标

| 命令                 | RedisClient类的方法                                          |
| -------------------- | ------------------------------------------------------------ |
| GEOADD               | func geoadd(key: String, longitude: Float64, latitude: Float64, member: String): Int64 |
|                      | func geoadd(key: String, memberCoordinateMap: Map&lt;String, GeoCoordinate&gt;, params: GeoAddParams): Int64 |
|                      | func geoadd(key: String, memberCoordinateMap: Map&lt;String, GeoCoordinate&gt;): Int64 |
| GEODIST              | func geodist(key: String, member1: String, member2: String, geoUnit: GeoUnit): ?Float64 |
|                      | func geodist(key: String, member1: String, member2: String): ?Float64 |
| GEOHASH              | func geohash(key: String, members: Array&lt;String&gt;): ArrayList&lt;?String&gt; |
| GEOPOS               | func geopos(key: String, members: Array&lt;String&gt;): ArrayList&lt;?GeoCoordinate&gt; |
| GEORADIUS            | func georadius(key: String, longitude: Float64, latitude: Float64,radius: Float64,geoUnit: GeoUnit,params: GeoRadiusParam): ArrayList&lt;GeoRadiusResponse&gt; |
|                      | func georadius(key: String,longitude: Float64, latitude: Float64,  radius: Float64,  geoUnit: GeoUnit): ArrayList&lt;GeoRadiusResponse&gt; |
|                      | func georadiusStore(key: String,longitude: Float64,latitude: Float64,radius: Float64,  geoUnit: GeoUnit, store: GeoRadiusStoreParam, params: GeoRadiusParam): Int64 |
|                      | func georadiusStore(key: String, longitude: Float64,latitude: Float64, radius: Float64,  geoUnit: GeoUnit, store: GeoRadiusStoreParam): Int64 |
| GEORADIUS_RO         | func georadiusReadonly( key: String, longitude: Float64,latitude: Float64, radius: Float64,geoUnit: GeoUnit, params: GeoRadiusParam ): ArrayList&lt;GeoRadiusResponse&gt; |
|                      | func georadiusReadonly(key: String,longitude: Float64, latitude: Float64, radius: Float64, geoUnit: GeoUnit): ArrayList&lt;GeoRadiusResponse&gt; |
| GEORADIUSBYMEMBER    | func georadiusByMember( key: String,member: String,radius: Float64,unit: GeoUnit,params: GeoRadiusParam): ArrayList&lt;GeoRadiusResponse&gt; |
|                      | func georadiusByMember( key: String, member: String,radius: Float64,unit: GeoUnit): ArrayList&lt;GeoRadiusResponse&gt; |
|                      | func georadiusByMemberStore(key: String,member: String,radius: Float64,unit: GeoUnit,store: GeoRadiusStoreParam,params: GeoRadiusParam): Int64 |
|                      | func georadiusByMemberStore(key: String,member: String,radius: Float64,unit: GeoUnit,store: GeoRadiusStoreParam): Int64 |
| GEORADIUSBYMEMBER_RO | func georadiusByMemberReadonly(key: String,member: String,radius: Float64,unit: GeoUnit,params: GeoRadiusParam): ArrayList&lt;GeoRadiusResponse&gt; |
|                      | func georadiusByMemberReadonly(key: String,member: String,radius: Float64,unit: GeoUnit): ArrayList&lt;GeoRadiusResponse&gt; |
| GEOSEARCH            | func geosearch(key: String, member: String, radius: Float64, unit: GeoUnit): ArrayList&lt;GeoRadiusResponse&gt; |
|                      | func geosearch(key: String, coord: GeoCoordinate, radius: Float64, unit: GeoUnit): ArrayList&lt;GeoRadiusResponse&gt; |
|                      | func geosearch(key: String, member: String, width: Float64, height: Float64, unit: GeoUnit): ArrayList&lt;GeoRadiusResponse&gt; |
|                      | func geosearch(key: String, coord: GeoCoordinate, width: Float64, height: Float64, unit: GeoUnit): ArrayList&lt;GeoRadiusResponse&gt; |
|                      | func geosearch(key: String, params: GeoSearchParam): ArrayList&lt;GeoRadiusResponse&gt; |
| GEOSEARCHSTORE       | func geosearchStore(dest: String, src: String, member: String, radius: Float64, unit: GeoUnit): Int64 |
|                      | func geosearchStore(dest: String, src: String, coord: GeoCoordinate, radius: Float64, unit: GeoUnit): Int64 |
|                      | func geosearchStore(dest: String, src: String, member: String, width: Float64, height: Float64, unit: GeoUnit): Int64 |
|                      | func geosearchStore(dest: String, src: String, params: GeoSearchParam): Int64 |
|                      | func geosearchStoreStoreDist(dest: String, src: String, params: GeoSearchParam): Int64 |
|                      | func geosearchStore(dest: String, src: String, coord: GeoCoordinate, width: Float64, height: Float64, unit: GeoUnit): Int64 |

#### Key命令

| 命令            | RedisClient类的方法                                          |
| --------------- | ------------------------------------------------------------ |
| EXISTS          | func exists(key: String): Bool                               |
|                 | func exists(keys: Array&lt;String&gt;): Int64                |
| PERSIST         | func persist(key: String): Int64                             |
| TYPE            | func Type(key: String): String                               |
| DUMP            | func dump(key: String): ?Array&lt;Byte&gt;                   |
| RESTORE         | func restore(key: String, ttl: Int64, serializedValue: Array&lt;Byte&gt;): String |
|                 | func restore(key: String, ttl: Int64, serializedValue: Array&lt;Byte&gt;, params: RestoreParams): String |
| EXPIRE          | func expire(key: String, seconds: Int64): Int64              |
|                 | func expire(key: String, seconds: Int64, expiryOption: ExpiryOption): Int64 |
| PEXPIRE         | func pexpire(key: String, milliseconds: Int64): Int64        |
|                 | func pexpire(key: String, milliseconds: Int64, expiryOption: ExpiryOption): Int64 |
| EXPIRETIME      | func expireTime(key: String): Int64                          |
| PEXPIRETIME     | func pexpireTime(key: String): Int64                         |
| EXPIREAT        | func expireAt(key: String, unixTime: Int64): Int64           |
|                 | func expireAt(key: String, unixTime: Int64, expiryOption: ExpiryOption): Int64 |
| PEXPIREAT       | func pexpireAt(key: String, millisecondsTimestamp: Int64): Int64 |
|                 | func pexpireAt(key: String, millisecondsTimestamp: Int64, expiryOption: ExpiryOption): Int64 |
| TTL             | func ttl(key: String): Int64                                 |
| PTTL            | func pttl(key: String): Int64                                |
| TOUCH           | func touch(key: String): Int64                               |
|                 | func touch(keys: Array&lt;String&gt;): Int64                 |
| SORT            | func sort(key: String): ArrayList&lt;String&gt;              |
|                 | func sort(key: String, dstkey: String): Int64                |
|                 | func sort(key: String, sortingParameters: SortingParams): ArrayList&lt;?String&gt; |
|                 | func sort(key: String, sortingParameters: SortingParams, dstkey: String): Int64 |
| SORT_RO         | func sortReadonly(key: String, sortingParams: SortingParams): ArrayList&lt;?String&gt; |
| DEL             | func del(key: String): Int64                                 |
|                 | func del(keys: Array&lt;String&gt;): Int64                   |
| UNLINK          | func unlink(key: String): Int64                              |
|                 | func unlink(keys: Array&lt;String&gt;): Int64                |
| COPY            | func copy(srcKey: String, dstkey: String, replace: Bool): Bool |
|                 | func copy(srcKey: String, dstKey: String, db: Int64, replace: Bool): Bool |
| MOVE            | func move(key: String, dbIndex: Int64): Int64                |
| RENAME          | func rename(oldkey: String, newkey: String): String          |
| RENAMENX        | func renamenx(oldkey: String, newkey: String): Int64         |
| MIGRATE         | func migrate(host: String, port: Int64, key: String, timeout: Int64): String |
|                 | func migrate(host: String, port: Int64, key: String, destinationDB: Int64, timeout: Int64): String |
|                 | func migrate(host: String, port: Int64, timeout: Int64, params: MigrateParams, keys: Array&lt;String&gt;): String |
|                 | func migrate(host: String,port: Int64,destinationDB: Int64,timeout: Int64,params: MigrateParams,keys: Array&lt;String&gt;): String |
| KEYS            | func keys(pattern: String): HashSet&lt;String&gt;            |
| SCAN            | func scan(cursor: String): (String, ArrayList&lt;String&gt;) |
|                 | func scan(cursor: String, params: ScanParams): (String, ArrayList&lt;String&gt;) |
| RANDOMKEY       | func randomKey(): ?String                                    |
| OBJECT REFCOUNT | func objectRefcount(key: String): ?Int64                     |
| OBJECT ENCODING | func objectEncoding(key: String): ?String                    |
| OBJECT IDLETIME | func objectIdletime(key: String): ?Int64                     |
| OBJECT FREQ     | func objectFreq(key: String): ?Int64                         |

#### Hash数据类型操作命令

| 命令         | RedisClient类的方法                                          |
| ------------ | ------------------------------------------------------------ |
| HSET         | func hset(key: String, field: String, value: String): Int64  |
|              | func hset(key: String, hash: Map&lt;String, String&gt;): Int64 |
| HGET         | func hget(key: String, field: String): ?String               |
| HSETNX       | func hsetnx(key: String, field: String, value: String): Int64 |
| HMSET        | func hmset(key: String, hash: Map&lt;String, String&gt;): String |
| HMGET        | func hmget(key: String, fields: Array&lt;String&gt;): ArrayList&lt;?String&gt; |
| HINCRBY      | func hincrBy(key: String, field: String, value: Int64): Int64 |
| HINCRBYFLOAT | func hincrByFloat(key: String, field: String, value: Float64): Float64 |
| HEXISTS      | func hexists(key: String, field: String): Bool               |
| HDEL         | func hdel(key: String, field: Array&lt;String&gt;): Int64    |
| HLEN         | func hlen(key: String): Int64                                |
| HKEYS        | func hkeys(key: String): HashSet&lt;String&gt;               |
| HVALS        | func hvals(key: String): ArrayList&lt;String&gt;             |
| HGETALL      | func hgetAll(key: String): HashMap&lt;String, String&gt;     |
| HRANDFIELD   | func hrandfield(key: String): ?String                        |
|              | func hrandfield(key: String, count: Int64): ArrayList&lt;String&gt; |
|              | func hrandfieldWithValues(key: String, count: Int64): ArrayList&lt;(String, String)&gt; |
| HSCAN        | func hscan(key: String, cursor: String): (String, ArrayList&lt;(String, String)&gt;) |
|              | func hscan(key: String, cursor: String, params: ScanParams): (String, ArrayList&lt;(String, String)&gt;) |
| HSTRLEN      | func hstrlen(key: String, field: String): Int64              |

#### List数据类型操作命令

| 命令       | RedisClient类的方法                                          |
| ---------- | ------------------------------------------------------------ |
| BLMOVE     | func blmove(source: String, destination: String, fromDir: ListDirection, toDir: ListDirection, timeout: Float64): ?String |
| BLMPOP     | func blmpop(timeout: Float64, direction: ListDirection, keys: Array&lt;String&gt;): ?(String, ArrayList&lt;String&gt;) |
|            | func blmpop(timeout: Float64, direction: ListDirection, count: Int64, keys: Array&lt;String&gt;): ?(String, ArrayList&lt;String&gt;) |
| BLPOP      | func blpop(timeout: Float64, keys: Array&lt;String&gt;): ?(String, String) |
|            | func blpop(timeout: Int64, keys: Array&lt;String&gt;): ?(String, String) |
| BRPOP      | func brpop(timeout: Int64, keys: Array&lt;String&gt;): ?(String, String) |
|            | func brpop(timeout: Float64, keys: Array&lt;String&gt;): ?(String, String) |
| BRPOPLPUSH | func brpoplpush(source: String, destination: String, timeout: Int64): ?String |
| LINDEX     | func lindex(key: String, index: Int64): ?String              |
| LINSERT    | func linsert(key: String, wherePos: ListPosition, pivot: String, value: String): Int64 |
| LLEN       | func llen(key: String): Int64                                |
| LMOVE      | func lmove(source: String, destination: String, fromDir: ListDirection, toDir: ListDirection): ?String |
| LMPOP      | func lmpop(direction: ListDirection, keys: Array&lt;String&gt;): ?(String, ArrayList&lt;String&gt;) |
|            | func lmpop(direction: ListDirection, count: Int64, keys: Array&lt;String&gt;): ?(String, ArrayList&lt;String&gt;) |
| LPOP       | func lpop(key: String): ?String                              |
|            | func lpop(key: String, count: Int64): ?ArrayList&lt;String&gt; |
| LPOS       | func lpos(key: String, value: String): ?Int64                |
|            | func lpos(key: String, value: String, args: LPosParams): ?Int64 |
|            | func lpos(key: String, value: String, args: LPosParams, count: Int64): ?ArrayList&lt;Int64&gt; |
| LPUSH      | func lpush(key: String, values: Array&lt;String&gt;): Int64  |
| LPUSHX     | func lpushx(key: String, values: Array&lt;String&gt;): Int64 |
| LRANGE     | func lrange(key: String, start: Int64, stop: Int64): ArrayList&lt;String&gt; |
| LREM       | func lrem(key: String, count: Int64, value: String): Int64   |
| LSET       | func lset(key: String, index: Int64, value: String): String  |
| LTRIM      | func ltrim(key: String, start: Int64, stop: Int64): String   |
| RPOP       | func rpop(key: String): ?String                              |
|            | func rpop(key: String, count: Int64): ?ArrayList&lt;String&gt; |
| RPOPLPUSH  | func rpoplpush(source: String, destination: String): ?String |
| RPUSH      | func rpush(key: String, values: Array&lt;String&gt;): Int64  |
| RPUSHX     | func rpushx(key: String, values: Array&lt;String&gt;): Int64 |

#### Set数据类型操作命令

| 命令        | RedisClient类的方法                                          |
| ----------- | ------------------------------------------------------------ |
| SADD        | func sadd(key: String, member: Array&lt;String&gt;): Int64   |
| SCARD       | func scard(key: String): Int64                               |
| SDIFF       | func sdiff(keys: Array&lt;String&gt;): HashSet&lt;String&gt; |
| SDIFFSTORE  | func sdiffstore(destination: String, keys: Array&lt;String&gt;): Int64 |
| SINTER      | func sinter(keys: Array&lt;String&gt;): HashSet&lt;String&gt; |
| SINTERCARD  | func sintercard(keys: Array&lt;String&gt;): Int64            |
|             | func sintercard(limit: Int64, keys: Array&lt;String&gt;): Int64 |
| SINTERSTORE | func sinterstore(destination: String, keys: Array&lt;String&gt;): Int64 |
| SISMEMBER   | func sismember(key: String, member: String): Bool            |
| SMISMEMBER  | func smismember(key: String, member: Array&lt;String&gt;): ArrayList&lt;Bool&gt; |
| SMEMBERS    | func smembers(key: String): HashSet&lt;String&gt;            |
| SMOVE       | func smove(source: String, destination: String, member: String): Bool |
| SPOP        | func spop(key: String): ?String                              |
|             | func spop(key: String, count: Int64): HashSet&lt;String&gt;  |
| SRANDMEMBER | func srandmember(key: String): ?String                       |
|             | func srandmember(key: String, count: Int64): ArrayList&lt;String&gt; |
| SREM        | func srem(key: String, member: Array&lt;String&gt;): Int64   |
| SUNION      | func sunion(keys: Array&lt;String&gt;): HashSet&lt;String&gt; |
| SUNIONSTORE | func sunionstore(destination: String, keys: Array&lt;String&gt;): Int64 |
| SSCAN       | func sscan(key: String, cursor: String): (String, ArrayList&lt;String&gt;) |
|             | func sscan(key: String, cursor: String, params: ScanParams): (String, ArrayList&lt;String&gt;) |

#### Stream数据类型操作命令

| 命令                  | RedisClient类的方法                                          |
| --------------------- | ------------------------------------------------------------ |
| XACK                  | func xack(key: String, group: String, ids: Array&lt;StreamEntryID&gt;): Int64 |
| XADD                  | func xadd(key: String, hash: Map&lt;String, String&gt;): ?StreamEntryID |
|                       | func xadd(key: String, id: StreamEntryID, hash: Map&lt;String, String&gt;): ?StreamEntryID |
|                       | func xadd(key: String, params: XAddParams, hash: Map&lt;String, String&gt;): ?StreamEntryID |
| XAUTOCLAIM            | func xautoclaim(key: String,group: String,consumerName: String,minIdleTime: Int64,start: StreamEntryID,params: XAutoClaimParams): (StreamEntryID, ArrayList&lt;StreamEntry&gt;) |
|                       | func xautoclaimJustId(key: String,group: String,consumerName: String,minIdleTime: Int64,start: StreamEntryID,params: XAutoClaimParams): (StreamEntryID, ArrayList&lt;StreamEntryID&gt;) |
| XCLAIM                | func xclaim(key: String,group: String,consumerName: String,minIdleTime: Int64,params: XClaimParams,ids: Array&lt;StreamEntryID&gt;): ArrayList&lt;StreamEntry&gt; |
|                       | func xclaimJustId(key: String,group: String,consumerName: String,minIdleTime: Int64,params: XClaimParams,ids: Array&lt;StreamEntryID&gt;): ArrayList&lt;StreamEntryID&gt; |
| XDEL                  | func xdel(key: String, ids: Array&lt;StreamEntryID&gt;): Int64 |
| XGROUP CREATE         | func xgroupCreate(key: String, groupName: String, makeStream: Bool): String |
|                       | func xgroupCreate(key: String, groupName: String, id: StreamEntryID, makeStream: Bool): String |
| XGROUP CREATECONSUMER | func xgroupCreateConsumer(key: String, groupName: String, consumerName: String): Bool |
| XGROUP DELCONSUMER    | func xgroupDelConsumer(key: String, groupName: String, consumerName: String): Int64 |
| XGROUP DESTROY        | func xgroupDestroy(key: String, groupName: String): Int64    |
| XGROUP SETID          | func xgroupSetID(key: String, groupName: String, id: StreamEntryID): String |
| XINFO CONSUMERS       | func xinfoConsumers(key: String, group: String): ArrayList&lt;StreamConsumerInfo&gt; |
| XINFO GROUPS          | func xinfoGroups(key: String): ArrayList&lt;StreamGroupInfo&gt; |
| XINFO STREAM          | func xinfoStream(key: String): StreamInfo                    |
|                       | func xinfoStreamFull(key: String): StreamFullInfo            |
|                       | func xinfoStreamFull(key: String, count: Int64): StreamFullInfo |
| XLEN                  | func xlen(key: String): Int64                                |
| XPENDING              | func xpending(key: String, groupName: String): StreamPendingSummary |
|                       | func xpending(key: String, groupName: String, params: XPendingParams): ArrayList&lt;StreamPendingEntry&gt; |
| XRANGE                | func xrange(key: String, start: ?StreamEntryID, end: ?StreamEntryID): ArrayList&lt;StreamEntry&gt; |
|                       | func xrange(key: String, start: ?StreamEntryID, end: ?StreamEntryID, count: Int64): ArrayList&lt;StreamEntry&gt; |
|                       | func xrange(key: String, start: String, end: String): ArrayList&lt;StreamEntry&gt; |
|                       | func xrange(key: String, start: String, end: String, count: Int64): ArrayList&lt;StreamEntry&gt; |
| XREAD                 | func xread(xreadParams: XReadParams, streams: Map&lt;String, StreamEntryID&gt;): ?ArrayList&lt;(String, ArrayList&lt;StreamEntry&gt;)&gt; |
| XREADGROUP            | func xreadGroup(groupName: String, consumer: String, params: XReadGroupParams, streams: Map&lt;String, StreamEntryID&gt;): ?ArrayList&lt;(String,  ArrayList&lt;StreamEntry&gt;)&gt; |
| XREVRANGE             | func xrevrange(key: String, end: ?StreamEntryID, start: ?StreamEntryID): ArrayList&lt;StreamEntry&gt; |
|                       | func xrevrange(key: String, end: ?StreamEntryID, start: ?StreamEntryID, count: Int64): ArrayList&lt;StreamEntry&gt; |
|                       | func xrevrange(key: String, end: String, start: String): ArrayList&lt;StreamEntry&gt; |
|                       | func xrevrange(key: String, end: String, start: String, count: Int64): ArrayList&lt;StreamEntry&gt; |
| XTRIM                 | func xtrim(key: String, maxLen: Int64, approximate: Bool): Int64 |
|                       | func xtrim(key: String, params: XTrimParams): Int64          |

#### String数据类型操作命令
| 命令        | RedisClient类的方法                                          |
| ----------- | ------------------------------------------------------------ |
| SET         | func set(key: String, value: String): String                 |
|             | func set(key: String, value: String, params: SetParams): ?String |
| GET         | func get(key: String): ?String                               |
|             | func setGet(key: String, value: String): ?String             |
|             | func setGet(key: String, value: String, params: SetParams): ?String |
| GETDEL      | func getDel(key: String): ?String                            |
| GETEX       | func getEx(key: String, params: GetExParams): ?String        |
| SETRANGE    | func setrange(key: String, offset: Int64, value: String): Int64 |
| GETRANGE    | func getrange(key: String, startOffset: Int64, endOffset: Int64): ?String |
| GETSET      | func getSet(key: String, value: String): ?String             |
| SETNX       | func setnx(key: String, value: String): Int64                |
| SETEX       | func setex(key: String, seconds: Int64, value: String): String |
| PSETEX      | func psetex(key: String, milliseconds: Int64, value: String): String |
| MGET        | func mget(keys: Array&lt;String&gt;): Array&lt;?String&gt;   |
| MSET        | func mset(keysvalues: Array&lt;String&gt;): String           |
| MSETNX      | func msetnx(keysvalues: Array&lt;String&gt;): Int64          |
| INCR        | func incr(key: String): Int64                                |
| INCRBY      | func incrBy(key: String, increment: Int64): Int64            |
| INCRBYFLOAT | func incrByFloat(key: String, increment: Float64): Float64   |
| DECR        | func decr(key: String): Int64                                |
| DECRBY      | func decrBy(key: String, decrement: Int64): Int64            |
| APPEND      | func append(key: String, value: String): Int64               |
| SUBSTR      | func substr(key: String, start: Int64, end: Int64): ?String  |
| STRLEN      | func strlen(key: String): Int64                              |

#### ZSet数据类型操作命令

| 命令             | RedisClient类的方法                                          |
| ---------------- | ------------------------------------------------------------ |
| BZMPOP           | func bzmpop(timeout: Int64, args: SortedSetOption, keys: Array&lt;String&gt;): ?(String, (String, Float64)) |
|                  | func bzmpop(timeout: Int64, args: SortedSetOption, count: Int64, keys: Array&lt;String&gt;): ?(String, ArrayList&lt;(String, Float64)&gt;) |
| BZPOPMAX         | func bzpopmax(timeout: Int64, keys: Array&lt;String&gt;): ?(String, (String, Float64)) |
| ZADD             | func zadd(key: String, score: Float64, member: String): ?Int64 |
|                  | func zadd(key: String, score: Float64, member: String, params: ZAddParams): ?Int64 |
|                  | func zadd(key: String, scoreMembers: Map&lt;String, Float64&gt;): ?Int64 |
|                  | func zadd(key: String, scoreMembers: Map&lt;String, Float64&gt;, params: ZAddParams): ?Int64 |
|                  | func zaddIncr(key: String, score: Float64, member: String, params: ZAddParams): ?Float64 |
| ZSCORE           | func zscore(key: String, member: String): ?Float64           |
| BZPOPMIN         | func bzpopmin(timeout: Int64, keys: Array&lt;String&gt;): ?(String, (String, Float64)) |
| ZCARD            | func zcard(key: String): Int64                               |
| ZCOUNT           | func zcount(key: String, min: Float64, max: Float64): Int64  |
|                  | func zcount(key: String, min: String, max: String): Int64    |
| ZDIFF            | func zdiff(keys: Array&lt;String&gt;): ArrayList&lt;String&gt; |
|                  | func zdiffWithScores(keys: Array&lt;String&gt;): ArrayList&lt;(String, Float64)&gt; |
| ZDIFFSTORE       | func zdiffstore(dstKey: String, keys: Array&lt;String&gt;): Int64 |
| ZRANGE           | func zrange(key: String, start: Int64, stop: Int64): ArrayList&lt;String&gt; |
|                  | func zrange(key: String, params: ZRangeParams): ArrayList&lt;String&gt; |
|                  | func zrangeWithScores(key: String, params: ZRangeParams): ArrayList&lt;(String, Float64)&gt; |
|                  | func zrangeWithScores(key: String, start: Int64, stop: Int64): ArrayList&lt;(String, Float64)&gt; |
| ZINCRBY          | func zincrby(key: String, increment: Float64, member: String): Float64 |
|                  | func zincrby(key: String, increment: Float64, member: String, params: ZIncrByParams): ?Float64 |
| ZINTER           | func zinter(params: ZParams, keys: Array&lt;String&gt;): ArrayList&lt;String&gt; |
|                  | func zinterWithScores(params: ZParams, keys: Array&lt;String&gt;): ArrayList&lt;(String, Float64)&gt; |
| ZINTERCARD       | func zintercard(keys: Array&lt;String&gt;): Int64            |
|                  | func zintercard(limit: Int64, keys: Array&lt;String&gt;): Int64 |
| ZINTERSTORE      | func zinterstore(dstKey: String, sets: Array&lt;String&gt;): Int64 |
|                  | func zinterstore(dstKey: String, params: ZParams, sets: Array&lt;String&gt;): Int64 |
| ZLEXCOUNT        | func zlexcount(key: String, min: String, max: String): Int64 |
| ZMPOP            | func zmpop(args: SortedSetOption, keys: Array&lt;String&gt;): ?(String, (String, Float64)) |
|                  | func zmpop(args: SortedSetOption, count: Int64, keys: Array&lt;String&gt;): ?(String, ArrayList&lt;(String, Float64)&gt;) |
| ZMSCORE          | func zmscore(key: String, members: Array&lt;String&gt;): ArrayList&lt;?Float64&gt; |
| ZPOPMAX          | func zpopmax(key: String): ?(String, Float64)                |
|                  | func zpopmax(key: String, count: Int64): ArrayList&lt;(String, Float64)&gt; |
| ZPOPMIN          | func zpopmin(key: String): ?(String, Float64)                |
|                  | func zpopmin(key: String, count: Int64): ArrayList&lt;(String, Float64)&gt; |
| ZRANDMEMBER      | func zrandmember(key: String): ?String                       |
|                  | func zrandmember(key: String, count: Int64): ArrayList&lt;String&gt; |
|                  | func zrandmemberWithScores(key: String, count: Int64): ArrayList&lt;(String, Float64)&gt; |
| ZRANGEBYLEX      | func zrangeByLex(key: String, min: String, max: String): ArrayList&lt;String&gt; |
|                  | func zrangeByLex(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList&lt;String&gt; |
| ZRANGEBYSCORE    | func zrangeByScore(key: String, min: String, max: String): ArrayList&lt;String&gt; |
|                  | func zrangeByScore(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList&lt;String&gt; |
|                  | func zrangeByScoreWithScores(key: String, min: String, max: String): ArrayList&lt;(String, Float64)&gt; |
|                  | func zrangeByScoreWithScores(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList&lt;(String, Float64)&gt; |
|                  | func zrangeByScore(key: String, min: Float64, max: Float64, offset: Int64, count: Int64): ArrayList&lt;String&gt; |
|                  | func zrangeByScore(key: String, min: Float64, max: Float64): ArrayList&lt;String&gt; |
|                  | func zrangeByScoreWithScores(key: String, min: Float64, max: Float64): ArrayList&lt;(String, Float64)&gt; |
|                  | func zrangeByScoreWithScores(key: String, min: Float64, max: Float64, offset: Int64, count: Int64): ArrayList&lt;(String, Float64)&gt; |
| ZRANGESTORE      | func zrangestore(dest: String, src: String, params: ZRangeParams): Int64 |
| ZRANK            | func zrank(key: String, member: String): ?Int64              |
|                  | func zrankWithScore(key: String, member: String): ?(Int64, Float64) |
| ZREM             | func zrem(key: String, members: Array&lt;String&gt;): Int64  |
| ZREMRANGEBYLEX   | func zremrangeByLex(key: String, min: String, max: String): Int64 |
| ZREMRANGEBYRANK  | func zremrangeByRank(key: String, start: Int64, stop: Int64): Int64 |
| ZREMRANGEBYSCORE | func zremrangeByScore(key: String, min: Float64, max: Float64): Int64 |
|                  | func zremrangeByScore(key: String, min: String, max: String): Int64 |
| ZREVRANGE        | func zrevrange(key: String, start: Int64, stop: Int64): ArrayList&lt;String&gt; |
|                  | func zrevrangeWithScores(key: String, start: Int64, stop: Int64): ArrayList&lt;(String, Float64)&gt; |
| ZREVRANGEBYLEX   | func zrevrangeByLex(key: String, min: String, max: String): ArrayList&lt;String&gt; |
|                  | func zrevrangeByLex(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList&lt;String&gt; |
| ZREVRANGEBYSCORE | func zrevrangeByScore(key: String, min: String, max: String): ArrayList&lt;String&gt; |
|                  | func zrevrangeByScore(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList&lt;String&gt; |
|                  | func zrevrangeByScoreWithScores(key: String, min: String, max: String): ArrayList&lt;(String, Float64)&gt; |
|                  | func zrevrangeByScoreWithScores(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList&lt;(String, Float64)&gt; |
|                  | func zrevrangeByScore(key: String, min: Float64, max: Float64): ArrayList&lt;String&gt; |
|                  | func zrevrangeByScore(key: String, min: Float64, max: Float64, offset: Int64, count: Int64): ArrayList&lt;String&gt; |
|                  | func zrevrangeByScoreWithScores(key: String, min: Float64, max: Float64): ArrayList&lt;(String, Float64)&gt; |
|                  | func zrevrangeByScoreWithScores(key: String, min: Float64, max: Float64, offset: Int64, count: Int64): ArrayList&lt;(String, Float64)&gt; |
| ZREVRANK         | func zrevrank(key: String, member: String): ?Int64           |
|                  | func zrevrankWithScore(key: String, member: String): ?(Int64, Float64) |
| ZSCAN            | func zscan(key: String, cursor: String): (String, ArrayList&lt;(String, Float64)&gt;) |
|                  | func zscan(key: String, cursor: String, params: ScanParams): (String, ArrayList&lt;(String, Float64)&gt;) |
| ZUNION           | func zunion(params: ZParams, keys: Array&lt;String&gt;): ArrayList&lt;String&gt; |
|                  | func zunionWithScores(params: ZParams, keys: Array&lt;String&gt;): ArrayList&lt;(String, Float64)&gt; |
| ZUNIONSTORE      | func zunionstore(dstKey: String, keys: Array&lt;String&gt;): Int64 |
|                  | func zunionstore(dstKey: String, params: ZParams, keys: Array&lt;String&gt;): Int64 |

#### HyperLogLog命令

| 命令    | RedisClient类的方法                                          |
| ------- | ------------------------------------------------------------ |
| PFADD   | func pfadd(key: String, elements: Array&lt;String&gt;): Bool |
| PFCOUNT | func pfcount(keys: Array&lt;String&gt;): Int64               |
| PFMERGE | func pfmerge(destkey: String, sourcekeys: Array&lt;String&gt;): String |

#### Script和Function命令

| 命令             | RedisClient类的方法                                          |
| ---------------- | ------------------------------------------------------------ |
| EVAL             | func eval(script: String): ?Any                              |
|                  | func eval(script: String, keyCount: Int64, params: Array&lt;String&gt;): ?Any |
|                  | func eval(script: String, keys: ArrayList&lt;String&gt;, args: ArrayList&lt;String&gt;): ?Any |
| EVAL_RO          | func evalReadonly(script: String, keys: ArrayList&lt;String&gt;, args: ArrayList&lt;String&gt;): ?Any |
| EVALSHA          | func evalsha(sha1: String): ?Any                             |
|                  | func evalsha(sha1: String, keyCount: Int64, params: Array&lt;String&gt;): ?Any |
|                  | func evalsha(sha1: String, keys: ArrayList&lt;String&gt;, args: ArrayList&lt;String&gt;): ?Any |
| EVALSHA_RO       | func evalshaReadonly(sha1: String, keys: ArrayList&lt;String&gt;, args: ArrayList&lt;String&gt;): ?Any |
| SCRIPT EXISTS    | func scriptExists(sha1: String): Bool                        |
|                  | func scriptExists(sha1: Array&lt;String&gt;): ArrayList&lt;Bool&gt; |
| SCRIPT FLUSH     | func scriptFlush(): String                                   |
|                  | func scriptFlush(flushMode: FlushMode): String               |
| SCRIPT KILL      | func scriptKill(): String                                    |
| SCRIPT LOAD      | func scriptLoad(script: String): String                      |
| FCALL            | func fcall(name: String, keys: ArrayList&lt;String&gt;, args: ArrayList&lt;String&gt;): ?Any |
| FCALL_RO         | func fcallReadonly(name: String, keys: ArrayList&lt;String&gt;, args: ArrayList&lt;String&gt;): ?Any |
| FUNCTION DELETE  | func functionDelete(libraryName: String): String             |
| FUNCTION DUMP    | func functionDump(): Array&lt;Byte&gt;                       |
| FUNCTION FLUSH   | func functionFlush(): String                                 |
|                  | func functionFlush(flushMode: FlushMode): String             |
| FUNCTION KILL    | func functionKill(): String                                  |
| FUNCTION LIST    | func functionList(): ArrayList&lt;LibraryInfo&gt;            |
|                  | func functionList(libraryNamePattern: String): ArrayList&lt;LibraryInfo&gt; |
|                  | func functionListWithCode(): ArrayList&lt;LibraryInfo&gt;    |
|                  | func functionListWithCode(libraryNamePattern: String): ArrayList&lt;LibraryInfo&gt; |
| FUNCTION LOAD    | func functionLoad(functionCode: String): String              |
|                  | func functionLoadReplace(functionCode: String): String       |
| FUNCTION RESTORE | func functionRestore(serializedValue: Array&lt;Byte&gt;): String |
|                  | func functionRestore(serializedValue: Array&lt;Byte&gt;, policy: FunctionRestorePolicy): String |
| FUNCTION STATS   | func functionStats(): FunctionStats                          |

#### Connection Mangement命令

| 命令  | RedisClient类的方法                               |
| ----- | ------------------------------------------------- |
| AUTH  | func auth(password: String): String               |
|       | func auth(user: String, password: String): String |
| ECHO  | func echo(message: String): String                |
| PING  | func ping(): String                               |
|       | func ping(message: String): String                |
| RESET | func reset(): String                              |

#### DataBase 命令

| 命令     | RedisClient类的方法                               |
| -------- | ------------------------------------------------- |
| SELECT   | func select(index: Int64): String                 |
| DBSIZE   | func dbSize(): Int64                              |
| FLUSHDB  | func flushDB(): String                            |
|          | func flushDB(flushMode: FlushMode): String        |
| FLUSHALL | func flushAll(): String                           |
|          | func flushAll(flushMode: FlushMode): String       |
| SWAPDB   | func swapDB(index1: Int64, index2: Int64): String |

#### ACL命令

| 命令        | RedisClient类的方法                                          |
| ----------- | ------------------------------------------------------------ |
| ACL WHOAMI  | func aclWhoAmI(): String                                     |
| ACL GENPASS | func aclGenPass(): String                                    |
|             | func aclGenPass(bits: Int64): String                         |
| ACL LIST    | func aclList(): ArrayList&lt;String&gt;                      |
| ACL USERS   | func aclUsers(): ArrayList&lt;String&gt;                     |
| ACL GETUSER | func aclGetUser(name: String): ?HashMap&lt;String, Any&gt;   |
| ACL SETUSER | func aclSetUser(name: String): String                        |
|             | func aclSetUser(name: String, rules: Array&lt;String&gt;): String |
| ACL DELUSER | func aclDelUser(names: Array&lt;String&gt;): Int64           |
| ACL CAT     | func aclCat(): ArrayList&lt;String&gt;                       |
|             | func aclCat(category: String): ArrayList&lt;String&gt;       |
| ACL LOG     | func aclLog(): ArrayList&lt;HashMap&lt;String, Any&gt;&gt;   |
|             | func aclLog(limit: Int64): ArrayList&lt;HashMap&lt;String, Any&gt;&gt; |
|             | func aclLogReset(): String                                   |
| ACL LOAD    | func aclLoad(): String                                       |
| ACL SAVE    | func aclSave(): String                                       |
| ACL DRYRUN  | func aclDryRun(username: String, commandName: String, args: Array&lt;String&gt;): String |
|             | func aclDryRun(username: String, redisCommand: RedisCommand): String |

#### CLIENT命令

| 命令            | RedisClient类的方法                                          |
| --------------- | ------------------------------------------------------------ |
| CLIENT KILL     | func clientKill(ipPort: String): String                      |
|                 | func clientKill(ip: String, port: Int64): String             |
|                 | func clientKill(params: ClientKillParams): Int64             |
| CLIENT GETNAME  | func clientGetname(): ?String                                |
| CLIENT LIST     | func clientList(): String                                    |
|                 | func clientList(clientType: ClientType): String              |
|                 | func clientList(clientIds: Array&lt;Int64&gt;): String       |
| CLIENT INFO     | func clientInfo(): String                                    |
| CLIENT SETINFO  | func clientSetInfo(attr: ClientAttributeOption, value: String): String |
| CLIENT SETNAME  | func clientSetname(name: String): String                     |
| CLIENT ID       | func clientId(): Int64                                       |
| CLIENT UNBLOCK  | func clientUnblock(clientId: Int64): Int64                   |
|                 | func clientUnblock(clientId: Int64, unblockType: UnblockType): Int64 |
| CLIENT PAUSE    | func clientPause(timeout: Int64): String                     |
|                 | func clientPause(timeout: Int64, mode: ClientPauseMode): String |
| CLIENT UNPAUSE  | func clientUnpause(): String                                 |
| CLIENT NO-EVICT | func clientNoEvictOn(): String                               |
|                 | func clientNoEvictOff(): String                              |
| CLIENT NO-TOUCH | func clientNoTouchOn(): String                               |
|                 | func clientNoTouchOff(): String                              |

#### CONFIG命令
| 命令             | RedisClient类的方法                                          |
| ---------------- | ------------------------------------------------------------ |
| CONFIG GET       | func configGet(patterns: Array&lt;String&gt;): HashMap&lt;String, String&gt; |
| CONFIG SET       | func configSet(parameter: String, value: String): String     |
|                  | func configSet(parameterValues: Array&lt;String&gt;): String |
|                  | func configSet(parameterValues: Map&lt;String, String&gt;): String |
| CONFIG RESETSTAT | func configResetStat(): String                               |
| CONFIG REWRITE   | func configRewrite(): String                                 |

#### Server Mangement命令

| 命令          | RedisClient类的方法                                 |
| ------------- | --------------------------------------------------- |
| SAVE          | func save(): String                                 |
| BGSAVE        | func bgsave(): String                               |
|               | func bgsaveSchedule(): String                       |
| BGREWRITEAOF  | func bgrewriteaof(): String                         |
| LASTSAVE      | func lastsave(): Int64                              |
| SHUTDOWN      | func shutdown(): Unit                               |
|               | func shutdown(saveMode: SaveMode): Unit             |
|               | func shutdown(shutdownParams: ShutdownParams): Unit |
|               | func shutdownAbort(): String                        |
| INFO          | func info(): String                                 |
|               | func info(section: String): String                  |
|               | func info(sections: Array&lt;String&gt;): String    |
| SLAVEOF       | func slaveof(host: String, port: Int64): String     |
|               | func slaveofNoOne(): String                         |
| REPLICAOF     | func replicaof(host: String, port: Int64): String   |
|               | func replicaofNoOne(): String                       |
| LOLWUT        | func lolwut(): String                               |
|               | func lolwut(lolwutParams: LolwutParams): String     |
| LATENCYDOCTOR | func latencyDoctor(): String                        |

#### 其他
| 命令          | RedisClient类的方法                                          |
| ------------- | ------------------------------------------------------------ |
| WAIT          | func wait(replicas: Int64, timeout: Int64): Int64            |
|               | func waitReplicas(replicas: Int64, timeout: Int64): Int64    |
| WAITAOF       | func waitAOF(numLocal: Int64, numReplicas: Int64, timeout: Int64): (Int64, Int64) |
| MEMORY DOCTOR | func memoryDoctor(): String                                  |
| MEMORY USAGE  | func memoryUsage(key: String): ?Int64                        |
|               | func memoryUsage(key: String, samples: Int64): ?Int64        |
| MEMORY PURGE  | func memoryPurge(): String                                   |
| MEMORY STATS  | func memoryStats(): MemoryStat                               |
| ROLE          | func role(): ReplicationRole                                 |

### 附录3. 发布订阅命令支持

| 命令         | RedisSubscriber类的方法                                   | 说明                   |
| ------------ | --------------------------------------------------------- | ---------------------- |
| SUBSCRIBE    | func subscribe(channels: Array&lt;String&gt;): Unit       | 非阻塞订阅             |
|              | func blockSubscribe(channels: Array&lt;String&gt;): Unit  | 阻塞订阅               |
| PSUBSCRIBE   | func psubscribe(patterns: Array&lt;String&gt;): Unit      | 模式匹配方式非阻塞订阅 |
|              | func blockPsubscribe(patterns: Array&lt;String&gt;): Unit | 模式匹配方式阻塞订阅   |
| UNSUBSCRIBE  | func unsubscribe(): Unit                                  |                        |
|              | func unsubscribe(channels: Array&lt;String&gt;): Unit     |                        |
| PUNSUBSCRIBE | func punsubscribe(): Unit                                 |                        |
|              | func punsubscribe(patterns: Array&lt;String&gt;): Unit    |                        |

### 附录4. 哨兵模式管理命令支持

| 命令                             | RedisClient类的方法                                          |
| -------------------------------- | ------------------------------------------------------------ |
| SENTINEL FAILOVER                | func sentinelFailover(masterName: String): String            |
| SENTINEL GET-MASTER-ADDR-BY-NAME | func sentinelGetMasterAddrByName(masterName: String): ?(String, String) |
| SENTINEL MASTER                  | func sentinelMaster(masterName: String): HashMap&lt;String, String&gt; |
| SENTINEL MASTERS                 | func sentinelMasters(): ArrayList&lt;HashMap&lt;String, String&gt;&gt; |
| SENTINEL MONITOR                 | func sentinelMonitor(masterName: String, ip: String, port: Int64, quorum: Int64): String |
| SENTINEL MYID                    | func sentinelMyId(): String                                  |
| SENTINEL REMOVE                  | func sentinelRemove(masterName: String): String              |
| SENTINEL REPLICAS                | func sentinelReplicas(masterName: String): ArrayList&lt;HashMap&lt;String, String&gt;&gt; |
| SENTINEL RESET                   | func sentinelReset(pattern: String): Float64                 |
| SENTINEL SENTINELS               | func sentinelSentinels(masterName: String): ArrayList&lt;HashMap&lt;String, String&gt;&gt; |
| SENTINEL SET                     | func sentinelSet(masterName: String, parameterMap: Map&lt;String, String&gt;): String |

### 附录5. 集群模式管理命令支持

| 命令                          | RedisClient类的方法                                          |
| ----------------------------- | ------------------------------------------------------------ |
| ASKING                        | func asking(): String                                        |
| CLUSTER ADDSLOTS              | func clusterAddSlots(slots: Array&lt;Int64&gt;): String      |
| CLUSTER ADDSLOTSRANGE         | func clusterAddSlotsRange(ranges: Array&lt;Int64&gt;): String |
| CLUSTER BUMPEPOCH             | func clusterBumpEpoch(): String                              |
| CLUSTER COUNT-FAILURE-REPORTS | func clusterCountFailureReports(nodeId: String): Int64       |
| CLUSTER COUNTKEYSINSLOT       | func clusterCountKeysInSlot(slot: Int64): Int64              |
| CLUSTER DELSLOTS              | func clusterDelSlots(slots: Array&lt;Int64&gt;): String      |
| CLUSTER DELSLOTSRANGE         | func clusterDelSlotsRange(ranges: Array&lt;Int64&gt;): String |
| CLUSTER FAILOVER              | func clusterFailover(): String                               |
|                               | func clusterFailover(failoverOption: ClusterFailoverOption): String |
| CLUSTER FORGET                | func clusterForget(nodeId: String): String                   |
| CLUSTER GETKEYSINSLOT         | func clusterGetKeysInSlot(slot: Int64, count: Int64): ArrayList&lt;String&gt; |
| CLUSTER INFO                  | func clusterInfo(): String                                   |
| CLUSTER KEYSLOT               | func clusterKeySlot(key: String): Int64                      |
| CLUSTER LINKS                 | func clusterLinks(): ArrayList&lt;HashMap&lt;String, ?Any&gt;&gt;        |
| CLUSTER MEET                  | func clusterMeet(ip: String, port: Int64): String            |
| CLUSTER MYID                  | func clusterMyId(): ?String                                  |
| CLUSTER MYSHARDID             | func clusterMyShardId(): String                              |
| CLUSTER NODES                 | func clusterNodes(): String                                  |
| CLUSTER REPLICAS              | func clusterReplicas(nodeId: String): ArrayList&lt;String&gt; |
| CLUSTER REPLICATE             | func clusterReplicate(nodeId: String): String                |
| CLUSTER RESET                 | func clusterReset(): String                                  |
|                               | func clusterReset(resetType: ClusterResetType): String       |
| CLUSTER SAVECONFIG            | func clusterSaveConfig(): String                             |
| CLUSTER SET-CONFIG-EPOCH      | func clusterSetConfigEpoch(configEpoch: Int64): String       |
| CLUSTER SETSLOT               | func clusterSetSlotImporting(slot: Int64, nodeId: String): String |
|                               | func clusterSetSlotMigrating(slot: Int64, nodeId: String): String |
|                               | func clusterSetSlotNode(slot: Int64, nodeId: String): String |
|                               | func clusterSetSlotStable(slot: Int64): String               |
| CLUSTER SHARDS                | func clusterShards(): ArrayList&lt;ClusterShardInfo&gt;      |
| CLUSTER SLOTS                 | func clusterSlots(): ArrayList&lt;Any&gt;                    |
| READONLY                      | func readonly(): String                                      |
| READWRITE                     | func readwrite(): String                                     |