/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client

/**
 * 集群模式下的命令执行器
 */
public open class ClusterCommandExecutor <: CommandExecutor {
    private static let logger: Logger = LoggerFactory.getLogger("redis-sdk")

    private let provider: ClusterConnectionProvider

    // 最大重试次数
    private let maxAttempts: Int64
    // 最大重试时间
    private let maxTotalRetriesDuration: Duration

    public init(
        clusterConnectionProvider: ClusterConnectionProvider,
        maxAttempts: Int64,
        maxTotalRetriesDuration: Duration
    ) {
        this.provider = clusterConnectionProvider
        this.maxAttempts = maxAttempts
        this.maxTotalRetriesDuration = maxTotalRetriesDuration
    }

    public override func getRespVersion(): ?ProtocolVersion {
        return provider.getRespVersion()
    }

    public open func getEndpoint(): ClientTcpEndpoint {
        return provider.getEndpoint()
    }

    public func getProvider(): ClusterConnectionProvider {
        return this.provider
    }

    public open func getEndpoint(command: RedisCommand): ClientTcpEndpoint {
        return provider.getEndpoint(command.getCommandArgs())
    }

    public open func getEndpoint(node: HostAndPort): ClientTcpEndpoint {
        return provider.getEndpoint(node)
    }

    public open func executeCommand(clientTcpEndPoint: ClientTcpEndpoint, command: RedisCommand): RedisMessage {
        return provider.executeCommand(clientTcpEndPoint, command)
    }

    public open func executeCommand(command: RedisCommand): RedisMessage {
        var redisMessage: ?RedisMessage = None
        let deadline = DateTime.now().addSeconds(maxTotalRetriesDuration.toSeconds())
        var redirect: ?RedisRedirectionException = None
        var lastException: ?Exception = None
        for (attemptsLeft in maxAttempts..0 : -1) {
            // 命令可能会被发送多次，需要调用clear方法清除上次结果
            command.clear()
            var endPoint: ?ClientTcpEndpoint = None
            try {
                if (let Some(redirect) <- redirect) {
                    let tempEndPoint = getEndpoint(redirect.getTargetNode())
                    endPoint = tempEndPoint
                    if (let Some(redirect) <- (redirect as RedisAskDataException)) {
                        executeAsking(tempEndPoint)
                    }
                } else {
                    endPoint = getEndpoint(command)
                }
                return executeCommand(endPoint.getOrThrow(), command)
            } catch (jnrcne: RedisClusterOperationException) {
                logger.error("Failure to execute command ${command.getCommandType().name()}, error msg: ${jnrcne.message}", jnrcne)
                throw jnrcne
            } catch (jre: RedisRedirectionException) {
                if (let Some(ex) <- lastException) {
                    if (let Some(ex) <- (ex as RedisRedirectionException)) {
                        lastException = jre
                    }
                } else {
                    lastException = jre
                }
                logger.warn("Redirected by server to ${jre.getTargetNode()}, error msg: ${jre.message}")
                redirect = jre
                // if MOVED redirection occurred
                if (let Some(jre) <- (jre as RedisMovedDataException)) {
                    try {
                        provider.renewSlotCache(endPoint.getOrThrow())
                    } catch (ex: Exception) {
                        if (ex is RedisException) {
                            throw ex
                        }
                        throw RedisException("Failure to renew the slot cache when moving to target node", ex)
                    }
                }
            } catch (ex: Exception) {
                lastException = ex
                if (logger.isDebugEnabled()) {
                    logger.warn(
                        "Failure to execute command ${command.getCommandType().name()}, will retry later",
                        ex
                    )
                } else {
                    logger.warn(
                        "Failure to execute command ${command.getCommandType().name()}, will retry later, error msg: ${ex.message}"
                    )
                }
                let reset = handleConnectionProblem(ex, attemptsLeft - 1, deadline)
                if (reset) {
                    redirect = None
                }
            }
        }
        var message = ""
        if (let Some(lastException) <- lastException) {
            message = "No more cluster attempts left, ${lastException.message}"
            throw RedisClusterOperationException(message, lastException)
        } else {
            message = "No more cluster attempts left."
            throw RedisClusterOperationException(message)
        }
    }

    public open func broadcastCommand(command: RedisCommand): RedisMessage {
        var connectionMap = provider.getConnectionMap()
        var entryIt = connectionMap.iterator()
        var reply: ?RedisMessage = None
        var bcastError = RedisBroadcastException()
        while (let Some((key, pool)) <- entryIt.next()) {
            // 命令可能会被发送多次，需要调用clear方法清除上次结果
            command.clear()
            var aReply = executeCommand(pool, command)
            if (let Some(message) <- (aReply as ErrorRedisMessage)) {
                bcastError.addReply(HostAndPort.toHostAndPort(key), message.getValue())
            } else if (let Some(message) <- (aReply as BulkErrorRedisMessage)) {
                bcastError.addReply(HostAndPort.toHostAndPort(key), message.getValue())
            } else {
                if (let Some(message) <- reply) {
                    if (message.toString() != aReply.toString()) {
                        bcastError.addReply(
                            HostAndPort.toHostAndPort(key),
                            "The return value is different from other nodes."
                        )
                    }
                } else {
                    reply = aReply
                }
            }
        }
        if (bcastError.getReplies().size > 0) {
            throw bcastError
        }
        if (let Some(message) <- reply) {
            return message
        } else {
            throw RedisBroadcastException(
                "Failure to broadcast command ${command.getCommandType().name()}, occured unexpected error.")
        }
    }

    /**
     * 处理连接异常
     */
    private func handleConnectionProblem(
        ex: Exception,
        attemptsLeft: Int64,
        deadline: DateTime
    ): Bool {
        if (!RecoverUtils.isTransportException(ex)) {
            // 非通信异常，直接抛出异常，不再重试
            if (let Some(redisEx) <- (ex as RedisException)) {
                throw redisEx
            } else {
                throw RedisException(ex.message, ex)
            }
        }

        if (this.maxAttempts < 3) {
            if (attemptsLeft == 0) {
                provider.renewSlotCache()
                return true
            }
            return false
        }

        let sleepTime = RecoverUtils.getBackoffSleepMillis(attemptsLeft, deadline)
        try {
            if (sleepTime > 0) {
                sleep(sleepTime * Duration.millisecond)
            }
        } catch (ex: Exception) {
            throw RedisException(ex.message, ex)
        }

        provider.renewSlotCache()
        return true
    }

    protected open func internalClose(): Unit {
        provider.close()
    }

    private func executeAsking(clientEndPoint: ClientTcpEndpoint): String {
        let command = RedisCommandBuilder.asking()
        let message = executeCommand(clientEndPoint, command)
        return command.getBuilder().build(message)
    }
}
