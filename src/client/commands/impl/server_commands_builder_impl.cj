/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client.commands.impl

public class ServerCommandsBuilderImpl {
    public func ping(): ParameterizedRedisCommand<String> {
        let command = ParameterizedRedisCommand(CommandType.PING, ResponseBuilderFactory.stringBuilder)
        return command
    }

    public func ping(message: String): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(message)
        let command = ParameterizedRedisCommand<String>(
            CommandType.PING,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func echo(message: String): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(message)
        let command = ParameterizedRedisCommand<String>(
            CommandType.ECHO,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func auth(password: String): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(password)
        let command = ParameterizedRedisCommand<String>(
            CommandType.AUTH,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func auth(user: String, password: String): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(user).add(password)
        let command = ParameterizedRedisCommand<String>(
            CommandType.AUTH,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func save(): ParameterizedRedisCommand<String> {
        let command = ParameterizedRedisCommand<String>(CommandType.SAVE, ResponseBuilderFactory.stringBuilder)
        return command
    }

    public func bgsave(): ParameterizedRedisCommand<String> {
        let command = ParameterizedRedisCommand<String>(CommandType.BGSAVE, ResponseBuilderFactory.stringBuilder)
        return command
    }

    public func bgsaveSchedule(): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.SCHEDULE)
        let command = ParameterizedRedisCommand<String>(
            CommandType.BGSAVE,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func bgrewriteaof(): ParameterizedRedisCommand<String> {
        let command = ParameterizedRedisCommand<String>(CommandType.BGREWRITEAOF, ResponseBuilderFactory.stringBuilder)
        return command
    }

    public func lastsave(): ParameterizedRedisCommand<Int64> {
        let command = ParameterizedRedisCommand<Int64>(CommandType.LASTSAVE, ResponseBuilderFactory.integerBuilder)
        return command
    }

    public func shutdownAbort(): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.ABORT)
        let command = ParameterizedRedisCommand<String>(
            CommandType.SHUTDOWN,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func info(): ParameterizedRedisCommand<String> {
        let command = ParameterizedRedisCommand<String>(CommandType.INFO, ResponseBuilderFactory.stringBuilder)
        return command
    }

    public func info(section: String): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(section)
        let command = ParameterizedRedisCommand<String>(
            CommandType.INFO,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func info(sections: Array<String>): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs()
        for (section in sections) {
            commandArgs.add(section)
        }
        let command = ParameterizedRedisCommand<String>(
            CommandType.INFO,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func slaveof(host: String, port: Int64): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(host).add(port)
        let command = ParameterizedRedisCommand<String>(
            CommandType.SLAVEOF,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func slaveofNoOne(): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.NO).add(CommandKeyword.ONE)
        let command = ParameterizedRedisCommand<String>(
            CommandType.SLAVEOF,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func replicaof(host: String, port: Int64): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(host).add(port)
        let command = ParameterizedRedisCommand<String>(
            CommandType.REPLICAOF,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func replicaofNoOne(): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.NO).add(CommandKeyword.ONE)
        let command = ParameterizedRedisCommand<String>(
            CommandType.REPLICAOF,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func wait(replicas: Int64, timeout: Int64): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().add(replicas).add(timeout)
        let command = ParameterizedRedisCommand<Int64>(
            CommandType.WAIT,
            commandArgs,
            ResponseBuilderFactory.integerBuilder
        )
        return command
    }

    public func waitReplicas(replicas: Int64, timeout: Int64): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().add(replicas).add(timeout)
        let command = ParameterizedRedisCommand<Int64>(
            CommandType.WAIT,
            commandArgs,
            ResponseBuilderFactory.integerBuilder
        )
        return command
    }

    public func waitAOF(numLocal: Int64, numReplicas: Int64, timeout: Int64): ParameterizedRedisCommand<(Int64, Int64)> {
        let commandArgs = CommandArgs().add(numLocal).add(numReplicas).add(timeout)
        let command = ParameterizedRedisCommand<(Int64, Int64)>(
            CommandType.WAITAOF,
            commandArgs,
            IntegerTupleResponseBuilder()
        )
        return command
    }

    public func lolwut(): ParameterizedRedisCommand<String> {
        let command = ParameterizedRedisCommand<String>(CommandType.LOLWUT, ResponseBuilderFactory.stringBuilder)
        return command
    }

    public func lolwut(lolwutParams: LolwutParams): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs()
        lolwutParams.buildArgs(commandArgs)
        let command = ParameterizedRedisCommand<String>(
            CommandType.LOLWUT,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func reset(): ParameterizedRedisCommand<String> {
        let command = ParameterizedRedisCommand<String>(CommandType.RESET, ResponseBuilderFactory.stringBuilder)
        return command
    }

    public func latencyDoctor(): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.DOCTOR)
        let command = ParameterizedRedisCommand<String>(
            CommandType.LATENCY,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }
}

public class IntegerTupleResponseBuilder <: ResponseBuilder<(Int64, Int64)> {
    public func build(message: RedisMessage): (Int64, Int64) {
        if (let Some(message) <- (message as ArraysRedisMessage)) {
            if (message.isNull()) {
                throw RedisException("Incompatible type, Nil/Null message is not the expect type")
            }

            let children = message.getValue()
            if (let Some(children) <- children) {
                if (children.size != 2) {
                    throw RedisException(
                        "Incompatible type, wrong size of ArraysRedisMessage, expect 2 but ${children.size}")
                }

                var key = -1
                let firstMessage = ResponseBuilderHelper.getRedisMessageOrThrow(
                    children.get(0),
                    RedisMessageType.INTEGERS
                )
                if (let Some(keyMesage) <- (firstMessage as IntegerRedisMessage)) {
                    key = keyMesage.getValue()
                } else {
                    throw RedisException(
                        "Incompatible type, expected first message's type: ${RedisMessageType.INTEGERS}, but ${firstMessage.getType()}"
                    )
                }

                var value = -1
                let secondMessage = ResponseBuilderHelper.getRedisMessageOrThrow(
                    children.get(1),
                    RedisMessageType.INTEGERS
                )
                if (let Some(valueMessage) <- (secondMessage as IntegerRedisMessage)) {
                    value = valueMessage.getValue()
                } else {
                    throw RedisException(
                        "Incompatible type, expected second message's type: ${RedisMessageType.INTEGERS}, but ${secondMessage.getType()}"
                    )
                }

                return (key, value)
            }

            throw RedisException("Incompatible type, nil arrays is not the expect type")
        } else if (let Some(message) <- (message as ErrorRedisMessage)) {
            throw RedisException(message.getValue())
        }

        throw RedisException("Incompatible type, ${message.getType()} is not the expect type")
    }
}
