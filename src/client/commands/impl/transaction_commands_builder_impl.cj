/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client.commands.impl

public class TransactionCommandsBuilderImpl <: TransactionCommandsBuilder {
    public func multi(): ParameterizedRedisCommand<String> {
        return ParameterizedRedisCommand<String>(
            CommandType.MULTI,
            CommandArgs(),
            ResponseBuilderFactory.stringBuilder
        )
    }

    public func exec(): ParameterizedRedisCommand<ArrayList<RedisMessage>> {
        return ParameterizedRedisCommand<ArrayList<RedisMessage>>(
            CommandType.EXEC,
            CommandArgs(),
            TransactionExecResponseBuilder()
        )
    }

    public func discard(): ParameterizedRedisCommand<String> {
        return ParameterizedRedisCommand<String>(
            CommandType.DISCARD,
            CommandArgs(),
            ResponseBuilderFactory.stringBuilder
        )
    }

    public func watch(keys: Array<String>): ParameterizedRedisCommand<String> {
        return ParameterizedRedisCommand<String>(
            CommandType.WATCH,
            CommandArgs().key(keys),
            ResponseBuilderFactory.stringBuilder
        )
    }

    public func unwatch(): ParameterizedRedisCommand<String> {
        return ParameterizedRedisCommand<String>(
            CommandType.UNWATCH,
            CommandArgs(),
            ResponseBuilderFactory.stringBuilder
        )
    }
}

public class TransactionExecResponseBuilder <: ResponseBuilder<ArrayList<RedisMessage>> {
    public func build(message: RedisMessage): ArrayList<RedisMessage> {
        if (let Some(message) <- (message as ArraysRedisMessage)) {
            if (let Some(val) <- message.getValue()) {
                return val
            } else if (message.isNull()) {
                let results = ArrayList<RedisMessage>()
                results.append(NullRedisMessage())
                return results
            }
        } else if (let Some(message) <- (message as NullRedisMessage)) {
            let results = ArrayList<RedisMessage>()
            results.append(message)
            return results
        }
        throw RedisException("Unsupported message type: ${message.getType()}")
    }
}
