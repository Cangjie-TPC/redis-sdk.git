/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_sdk.client

import std.collection.ArrayList
import std.time.{Duration, DurationExtension}

/**
 * Sentinel高可用集群的配置
 */
public class SentinelRedisClientConfig <: RedisClientConfigBase & Hashable & Equatable<SentinelRedisClientConfig> {
    private let masterNameVal: String
    private let sentinelHostAndPorts = ArrayList<HostAndPort>()

    private var sentinelUserVal: ?String = None

    private var sentinelPasswordVal: ?Array<Byte> = None

    private var subscribeRetryWaitTimeVal = Duration.second * 5

    // 最大重试次数
    private var maxAttemptsVal: Int64 = 1

    // 最大重试时间
    private var maxTotalRetriesDurationVal: Duration = Duration.second * 30

    // 延迟关闭SentinelManager(用于维护哨兵模式的集群状态的)的时间, 在该期间内SentinelManager再次被引用则取消关闭
    private var closeSentinelManagerDelayDurationVal = Duration.second * 10

    public init(masterName: String) {
        this.masterNameVal = masterName
    }

    public prop masterName: String {
        get() {
            return masterNameVal
        }
    }

    public func addSentinelHostAndPort(hostAndPort: HostAndPort) {
        if (!sentinelHostAndPorts.contains(hostAndPort)) {
            sentinelHostAndPorts.append(hostAndPort)
        }

        return this
    }

    public func addSentinelHostAndPorts(hostAndPorts: Array<HostAndPort>) {
        for (hostAndPort in hostAndPorts) {
            addSentinelHostAndPort(hostAndPort)
        }

        return this
    }

    public func addSentinelHostAndPorts(hostAndPorts: ArrayList<HostAndPort>) {
        for (hostAndPort in hostAndPorts) {
            addSentinelHostAndPort(hostAndPort)
        }

        return this
    }

    public func addSentinelHostAndPort(host: String, port: UInt16) {
        let hostAndPort = HostAndPort(host, port)
        return addSentinelHostAndPort(hostAndPort)
    }

    public func getSentinelHostAndPorts(): ArrayList<HostAndPort> {
        return this.sentinelHostAndPorts
    }

    public mut prop sentinelUser: ?String {
        get() {
            return sentinelUserVal
        }
        set(value) {
            this.sentinelUserVal = value
        }
    }

    public mut prop sentinelPassword: ?Array<Byte> {
        get() {
            return sentinelPasswordVal
        }
        set(value) {
            this.sentinelPasswordVal = value
        }
    }

    public mut prop subscribeMasterRetryWaitTime: Duration {
        get() {
            return subscribeRetryWaitTimeVal
        }
        set(value) {
            this.subscribeRetryWaitTimeVal = value
        }
    }

    public mut prop maxAttempts: Int64 {
        get() {
            return maxAttemptsVal
        }
        set(value) {
            this.maxAttemptsVal = value
        }
    }

    public mut prop maxTotalRetriesDuration: Duration {
        get() {
            return maxTotalRetriesDurationVal
        }
        set(value) {
            this.maxTotalRetriesDurationVal = value
        }
    }

    public mut prop closeSentinelManagerDelayDuration: Duration {
        get() {
            return closeSentinelManagerDelayDurationVal
        }
        set(value) {
            this.closeSentinelManagerDelayDurationVal = value
        }
    }

    public override func configTcpEndpointOptions(endpoint: ClientEndpointConfig) {
        super.configTcpEndpointOptions(endpoint)
        // 哨兵实例的Redis连接带认证等状态，所以每个RedisClient只使用一个TCP连接
        endpoint.minConnections = 0
        endpoint.maxConnections = 1
    }

    @OverflowWrapping
    public open func hashCode(): Int64 {
        let prime = 53
        var hashCode = super.computeHashCode()
        hashCode = hashCode + prime * masterNameVal.hashCode()

        // 先排序再计算HashCode
        let sortedHostAndPorts = ArrayList<HostAndPort>()
        sortedHostAndPorts.appendAll(sentinelHostAndPorts)
        sortedHostAndPorts.sort()
        for (hostAndPort in sortedHostAndPorts) {
            hashCode = hashCode + prime * hostAndPort.hashCode()
        }

        if (let Some(sentinelUser) <- sentinelUserVal) {
            hashCode = hashCode + prime * sentinelUser.hashCode()
        }

        if (let Some(sentinelPassword) <- sentinelPasswordVal) {
            hashCode = hashCode + prime * String.fromUtf8(sentinelPassword).hashCode()
        }

        hashCode = hashCode + prime * subscribeRetryWaitTimeVal.hashCode()
        hashCode = hashCode + prime * maxAttemptsVal.hashCode()
        hashCode = hashCode + prime * maxTotalRetriesDurationVal.hashCode()
        hashCode = hashCode + prime * closeSentinelManagerDelayDurationVal.hashCode()
        return hashCode
    }

    public operator override func ==(other: SentinelRedisClientConfig): Bool {
        if (masterNameVal != other.masterNameVal) {
            return false
        }

        if (!checkSameHostAndPorts(sentinelHostAndPorts, other.sentinelHostAndPorts)) {
            return false
        }

        if (sentinelUserVal != other.sentinelUserVal) {
            return false
        }

        var myPassword: ?String = None
        var otherPassword: ?String = None
        if (let Some(password) <- sentinelPasswordVal) {
            myPassword = String.fromUtf8(password)
        }

        if (let Some(otherPass) <- other.sentinelPasswordVal) {
            otherPassword = String.fromUtf8(otherPass)
        }

        if (myPassword != otherPassword) {
            return false
        }

        if (subscribeRetryWaitTimeVal != other.subscribeRetryWaitTimeVal) {
            return false
        }

        if (maxAttemptsVal != other.maxAttemptsVal) {
            return false
        }

        if (maxTotalRetriesDurationVal != other.maxTotalRetriesDurationVal) {
            return false
        }

        if (closeSentinelManagerDelayDurationVal != other.closeSentinelManagerDelayDurationVal) {
            return false
        }

        return super.checkEquals(other)
    }

    public operator override func !=(other: SentinelRedisClientConfig): Bool {
        if (masterNameVal != other.masterNameVal) {
            return true
        }

        if (!checkSameHostAndPorts(sentinelHostAndPorts, other.sentinelHostAndPorts)) {
            return true
        }

        if (sentinelUserVal != other.sentinelUserVal) {
            return true
        }

        var myPassword: ?String = None
        var otherPassword: ?String = None
        if (let Some(password) <- sentinelPasswordVal) {
            myPassword = String.fromUtf8(password)
        }

        if (let Some(otherPass) <- other.sentinelPasswordVal) {
            otherPassword = String.fromUtf8(otherPass)
        }

        if (myPassword != otherPassword) {
            return true
        }

        if (subscribeRetryWaitTimeVal != other.subscribeRetryWaitTimeVal) {
            return true
        }

        if (maxAttemptsVal != other.maxAttemptsVal) {
            return true
        }

        if (maxTotalRetriesDurationVal != other.maxTotalRetriesDurationVal) {
            return true
        }

        if (closeSentinelManagerDelayDurationVal != other.closeSentinelManagerDelayDurationVal) {
            return true
        }

        return !super.checkEquals(other)
    }

    private static func checkSameHostAndPorts(
        hostAndPorts: ArrayList<HostAndPort>,
        otherHostAndPorts: ArrayList<HostAndPort>
    ): Bool {
        if (hostAndPorts.size != otherHostAndPorts.size) {
            return false
        }

        let sortedHostAndPorts = ArrayList<HostAndPort>()
        sortedHostAndPorts.appendAll(hostAndPorts)
        sortedHostAndPorts.sort()

        let otherSortedHostAndPorts = ArrayList<HostAndPort>()
        otherSortedHostAndPorts.appendAll(otherHostAndPorts)
        otherSortedHostAndPorts.sort()

        return sortedHostAndPorts == otherSortedHostAndPorts
    }

    public func clone(): SentinelRedisClientConfig {
        let clientConfig = SentinelRedisClientConfig(this.masterName)
        this.cloneTo(clientConfig)
        clientConfig.sentinelHostAndPorts.appendAll(this.sentinelHostAndPorts.clone())
        clientConfig.sentinelUser = this.sentinelUser
        clientConfig.sentinelPassword = this.sentinelPassword
        clientConfig.subscribeMasterRetryWaitTime = this.subscribeMasterRetryWaitTime
        clientConfig.maxAttempts = this.maxAttempts
        clientConfig.maxTotalRetriesDuration = this.maxTotalRetriesDuration
        clientConfig.closeSentinelManagerDelayDuration = this.closeSentinelManagerDelayDuration
        return clientConfig
    }
}
