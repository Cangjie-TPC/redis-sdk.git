/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_client_unit_test

import std.unittest.testmacro.*

@Test
public class ServerCommandsTest {
    private var redisClient: ?RedisClient = None<RedisClient>

    @BeforeAll
    protected func beforeAll(): Unit {
        redisClient = TestUtils.getRedisClient()

        let client = redisClient.getOrThrow()
        try {
            println("CONFIG SET latency-monitor-threshold 3")
            let result = client.configSet("latency-monitor-threshold", "3")
            println(result)

            @Assert(result, "OK")
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testPing(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("PING")
            let result = client.ping()
            println(result)

            @Assert(result, "PONG")
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testPingMessage(): Unit {
        let client = redisClient.getOrThrow()
        let message = "MyMessage"
        try {
            println("PING")
            let result = client.ping(message)
            println(result)

            @Assert(result, message)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testEcho(): Unit {
        let client = redisClient.getOrThrow()
        let message = "MyMessage"
        try {
            println("PING")
            let result = client.echo(message)
            println(result)

            @Assert(result, message)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testAuth(): Unit {
        let client = redisClient.getOrThrow()
        try {
            if (let Some(password) <- TestUtils.getPassword()) {
                println("AUTH ********")
                let result = client.auth(password)
                println(result)

                @Assert(result, "OK")
            }
            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testAuthWithUser(): Unit {
        let client = redisClient.getOrThrow()
        let user = "newuser"
        let password = "newpasword"
        try {
            client.aclSetUser(user, "allkeys", "on", "+@all", ">${password}")

            println("AUTH ${user} ********")
            let result = client.auth(user, password)
            println(result)

            @Assert(result, "OK")
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            try {
                // 切换为default用户删除newuser，否则连接会断开
                if (let Some(password) <- TestUtils.getPassword()) {
                    let result = client.auth(password)
                } else {
                    throw Exception("No password if provided")
                }

                client.aclDelUser(user)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    @TestCase
    public func testSave(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("SAVE")
            let result = client.save()
            println(result)

            @Assert(result, "OK")
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testBgsave(): Unit {
        let client = redisClient.getOrThrow()
        try {
            sleep(Duration.second * 5)
            println("BGSAVE")
            let result = client.bgsave()
            println(result)

            @Assert(result, "Background saving started")
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testBgsaveSchedule(): Unit {
        let client = redisClient.getOrThrow()
        try {
            sleep(Duration.second * 5)
            println("BGSAVE SCHEDULE")
            let result = client.bgsave()
            println(result)

            if (result == "Background saving started" || result == "Background saving scheduled") {
                @Assert(true)
            } else {
                @Assert(false)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testBgrewriteaof(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("BGREWRITEAOF")
            let result = client.bgrewriteaof()
            println(result)

            if (result == "Background append only file rewriting started" || result ==
                "Background append only file rewriting scheduled") {
                @Assert(true)
            } else {
                @Assert(false)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testLastsave(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("LASTSAVE")
            let result = client.lastsave()
            println(result)
            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testInfo(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("INFO")
            let result = client.info()
            println(result)
            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testInfoWithSection(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("INFO server")
            let result = client.info("server")
            println(result)
            if (result.contains("redis_version:")) {
                @Assert(true)
            } else {
                @Assert(false)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testInfoWithSections(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("INFO server memory")
            let result = client.info("server", "memory")
            println(result)

            if (result.contains("redis_version:") && result.contains("memory:")) {
                @Assert(true)
            } else {
                @Assert(false)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testLolwut(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("LOLWUT")
            let result = client.lolwut()
            println(result)
            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testLolwutWithVersion(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("LOLWUT version 5")
            let parmas = LolwutParams()
            parmas.version(5)
            let result = client.lolwut(parmas)
            println(result)
            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testLatencyDoctor(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("LATENCY DOCTOR")
            let result = client.latencyDoctor()
            println(result)
            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            try {
                println("CONFIG SET latency-monitor-threshold 3000")
                let result = client.configSet("latency-monitor-threshold", "3000")
                println(result)
            } catch (ex: Exception) {
                ex.printStackTrace()
                @Assert(false)
            }
        }
    }

    @TestCase
    public func testSlaveofNoOne(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- (TestUtils.getRedisClientConfig() as ClusterRedisClientConfig)) {
            @Assert(true)
            return
        }
        try {
            println("SLAVEOF NO ONE")
            let result = client.slaveofNoOne()
            println(result)
            @Assert(result, "OK")
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testReplicaofNoOne(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- (TestUtils.getRedisClientConfig() as ClusterRedisClientConfig)) {
            @Assert(true)
            return
        }
        try {
            println("REPLICAOF NO ONE")
            let result = client.replicaofNoOne()
            println(result)
            @Assert(result, "OK")
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testShutdownAbort(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("SHUTDOWN ABORT")
            let result = client.shutdownAbort()
            println(result)
            @Assert(false)
        } catch (ex: Exception) {
            ex.printStackTrace()
            if (ex.message.startsWith("ERR No shutdown in progress")) {
                @Assert(true)
            } else {
                @Assert(false)
            }
        }
    }

    // func slaveof(host: String, port: Int64): String

    // func replicaof(host: String, port: Int64): String

    // func wait(replicas: Int64, timeout: Int64): Int64

    // func waitReplicas(replicas: Int64, timeout: Int64): Int64

    // func waitAOF(numLocal: Int64, numReplicas: Int64, timeout: Int64): (Int64, Int64)

}
