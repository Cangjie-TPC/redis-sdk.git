/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis_client_unit_test

import std.unittest.testmacro.*

@Test
public class SentinelCommandsTest {
    private var sentinelConfig: ?SentinelRedisClientConfig = None<SentinelRedisClientConfig>

    private var sentinelInstanceClient: ?RedisClient = None<RedisClient>

    @BeforeAll
    protected func beforeAll(): Unit {
        if (let Some(sentinelConfig) <- (TestUtils.getUnifiedRedisClient().getClientConfig() as SentinelRedisClientConfig)) {
            this.sentinelConfig = sentinelConfig

            let sentinelInstanceConfig = RedisClientConfig()
            sentinelConfig.cloneTo(sentinelInstanceConfig)
            if (sentinelConfig.getSentinelHostAndPorts().size > 0) {
                let hostAndPort = sentinelConfig.getSentinelHostAndPorts()[0]
                // 使用Sentinel实例的用户名、密码、主机名和端口
                sentinelInstanceConfig.user = sentinelConfig.sentinelUser
                sentinelInstanceConfig.password = sentinelConfig.sentinelPassword
                sentinelInstanceConfig.host = hostAndPort.host
                sentinelInstanceConfig.port = hostAndPort.port
                let builder = RedisClientBuilder.builder(sentinelInstanceConfig)
                sentinelInstanceClient = builder.build()
            }
        }
    }

    @AfterAll
    protected func afterAll(): Unit {
        if (let Some(sentinelInstanceClient) <- sentinelInstanceClient) {
            sentinelInstanceClient.close()
        }
    }

    @TestCase
    public func testMasterAndMasters(): Unit {
        if (sentinelConfig.isNone()) {
            @Assert(true)
            return
        }

        let client = sentinelInstanceClient.getOrThrow()
        var runId: ?String = None
        var port: ?String = None
        try {
            println("SENTINEL MASTER ${sentinelConfig.getOrThrow().masterName}")
            let details = client.sentinelMaster(sentinelConfig.getOrThrow().masterName)
            println(details)

            @Assert(sentinelConfig.getOrThrow().masterName, details.get("name"))
            runId = details.get("runid")
            println(runId)
            port = details.get("port")
            println(port)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }

        try {
            println("SENTINEL MASTERS ${sentinelConfig.getOrThrow().masterName}")
            let details = client.sentinelMasters()[0]
            println(details)

            @Assert(sentinelConfig.getOrThrow().masterName, details.get("name"))
            @Assert(runId, details.get("runid"))
            @Assert(port, details.get("port"))
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testMyIdAndSentinels() {
        if (sentinelConfig.isNone()) {
            @Assert(true)
            return
        }

        let client = sentinelInstanceClient.getOrThrow()
        let idToPort = HashMap<String, String>()
        try {
            println("SENTINEL MYID")
            let myid = client.sentinelMyId()
            println(myid)

            if (let Some(config) <- (client.getClientConfig() as RedisClientConfig)) {
                idToPort.put(myid, config.port.getOrThrow().toString())
            }

            println("SENTINEL SENTINELS ${sentinelConfig.getOrThrow().masterName}")
            let detailList = client.sentinelSentinels(sentinelConfig.getOrThrow().masterName)
            println(detailList)

            for (detail in detailList) {
                let port = detail.get("port")
                let id = detail.get("runid").getOrThrow()
                if (id == myid) {
                    @Assert(idToPort.get(id), port)
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testReplicas() {
        if (sentinelConfig.isNone()) {
            @Assert(true)
            return
        }

        let client = sentinelInstanceClient.getOrThrow()
        try {
            println("SENTINEL REPLICAS ${sentinelConfig.getOrThrow().masterName}")
            let detailsList = client.sentinelReplicas(sentinelConfig.getOrThrow().masterName)
            println(detailsList)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testSentinelSet() {
        if (sentinelConfig.isNone()) {
            @Assert(true)
            return
        }

        try {
            let parameterMap = HashMap<String, String>()
            parameterMap.put("down-after-milliseconds", "1234")
            parameterMap.put("parallel-syncs", "3")
            parameterMap.put("quorum", "2")

            let client = sentinelInstanceClient.getOrThrow()
            println("SENTINEL SET ${sentinelConfig.getOrThrow().masterName} ${parameterMap}")
            let status = client.sentinelSet(sentinelConfig.getOrThrow().masterName, parameterMap)
            println(status)

            println("SENTINEL MASTERS")
            let masters = client.sentinelMasters()
            println(masters)

            for (master in masters) {
                if (master.get("name").getOrThrow() == sentinelConfig.getOrThrow().masterName) {
                    @Assert("1234",master.get("down-after-milliseconds"))
                    @Assert("3",master.get("parallel-syncs"))
                    @Assert("2",master.get("quorum"))
                    return
                }
            }
            @Assert(false)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }
}
